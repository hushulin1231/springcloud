package com.cn.customer.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cn.customer.common.utils.PropertiesUtil;
import com.cn.customer.common.utils.R;
import com.cn.customer.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@Configuration
public class TestController {
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private PropertiesUtil propertiesUtil;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello(String name) {
        String url=propertiesUtil.url+"hello?name="+name;
        return restTemplate.getForEntity(url,String.class).getBody();
    }


    //新增用户
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public JSONObject add(UserEntity userEntity) {
        String url=propertiesUtil.url+"user/add";
        //添加参数,因为HttpEntity里面的参数是MultiValueMap类型的，所以使用这个map集合
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("username",userEntity.getUsername());
        map.add("password",userEntity.getPassword());
        map.add("sex",userEntity.getSex());
        ResponseEntity<String> response = restTemplate.postForEntity(url, map, String.class);
        JSONObject jsonObject = JSON.parseObject(response.getBody());
        return jsonObject;
    }



}
