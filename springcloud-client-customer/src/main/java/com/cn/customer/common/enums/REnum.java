package com.cn.customer.common.enums;

import lombok.Getter;

/**
   接口返回枚举
 */
@Getter
public enum REnum {
    /**
     * 成功
     */
    OK(200, "成功"),

    /**
     * 需要登录
     */
    UN_LOGIN(401, "需要登录"),

    /**
     * 没有权限
     */
    UN_AUTH(403, "没有权限"),

    /**
     * 不存在
     */
    NOT_FOUND(404, "不存在"),

    /**
     * 错误
     */
    ERROR(500, "未知错误，请联系管理员。");

    private int code;
    private String msg;

    REnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static REnum getEnum(int code) {
        for (REnum item : REnum.values()) {
            if (item.getCode() == code) {
                return item;
            }
        }
        return null;
    }
}
