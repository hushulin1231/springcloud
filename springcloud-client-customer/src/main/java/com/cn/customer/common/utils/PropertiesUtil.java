package com.cn.customer.common.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
public class PropertiesUtil {
    //调用provider服务的接口地址
    @Value("${url}")
    public String url;
}
