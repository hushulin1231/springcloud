package com.cn.customer.entity;

import lombok.Data;

@Data
public class UserEntity  {

    //用户名
    private String username;

    //密码
    private String password;

    //性别
    private String sex;

}
