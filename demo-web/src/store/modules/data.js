export default {
  namespaced: true,
  state: {
    logoConfig: {title: 'BOOT开发平台'}, // LOGO相关配置
    dict: [], // 数据字典数据
    orgType: [], // 组织类型
    enums: {}, // 枚举
  },
  mutations: {
    updateLogoConfig (state, data) {
      document.title = document.title.replace(state.logoConfig.title, (data && data.title) || state.logoConfig.title)
      state.logoConfig = {
        ...(data || state.logoConfig),
        title: (data && data.title) || state.logoConfig.title
      }
    },
    updateDict (state, data) {
      state.dict = data
    },
    updateOrgType (state, data) {
      state.orgType = data
    },
    updateEnums (state, data) {
      state.enums = data
    }
  },
  getters: {
    logoConfig: (state) => {
      return state.logoConfig
    },
    dict: (state) => {
      return state.dict
    },
    orgType: (state) => {
      return state.orgType
    },
    enums: (state) => {
      return state.enums
    }
  }
}
