export default {
  namespaced: true,
  state: {
    /* 通用 */
    globalLoading: false, // 全局加载状态
    documentClientHeight: 0, // 页面文档可视高度(随窗口改变大小)
    contentIsNeedRefresh: false, // 内容, 是否需要刷新

    /* 配置页面 */
    topNavbarEnabled: localStorage.getItem('topNavbarEnabled') || 'false', // 是否启用顶部导航
    navbarLayoutType: localStorage.getItem('navbarLayoutType') || 'default', // 导航条, 布局风格, defalut(默认) / inverse(反向)
    sidebarLayoutSkin: localStorage.getItem('sidebarLayoutSkin') || 'dark', // 侧边栏, 布局皮肤, light(浅色) / dark(黑色)

    /* 导航栏 */
    topNavbarSelected: '0', // 顶部导航栏选中
    sidebarFold: false, // 侧边栏, 折叠状态
    menuList: [], // 侧边栏, 菜单数据
    menuActiveName: '', // 当前侧边栏
    mainTabs: [], // 主入口标签页
    mainTabsActiveName: '' // 当前主入口标签页
  },
  mutations: {
    /* 通用 */
    updateGlobalLoading (state, status) {
      state.globalLoading = status
    },
    updateDocumentClientHeight (state, height) {
      state.documentClientHeight = height
    },
    updateContentIsNeedRefresh (state, status) {
      state.contentIsNeedRefresh = status
    },

    /* 配置页面 */
    updateTopNavbarEnabled (state, status) {
      localStorage.setItem('topNavbarEnabled', status)
      state.topNavbarEnabled = status
    },
    updateNavbarLayoutType (state, type) {
      localStorage.setItem('navbarLayoutType', type)
      state.navbarLayoutType = type
    },
    updateSidebarLayoutSkin (state, skin) {
      localStorage.setItem('sidebarLayoutSkin', skin)
      state.sidebarLayoutSkin = skin
    },

    /* 导航栏 */
    updateTopNavbarSelected (state, index) {
      state.topNavbarSelected = index
    },
    updateSidebarFold (state, fold) {
      state.sidebarFold = fold
    },
    updateMenuList (state, list) {
      state.menuList = list
    },
    updateMenuActiveName (state, name) {
      state.menuActiveName = name
    },
    updateMainTabs (state, tabs) {
      state.mainTabs = tabs
    },
    updateMainTabsActiveName (state, name) {
      state.mainTabsActiveName = name
    }
  },
  getters: {
    documentClientHeight: (state) => {
      return state.documentClientHeight
    },
    globalLoading: (state) => {
      return state.globalLoading
    }
  }
}
