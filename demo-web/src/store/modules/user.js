import { doGet } from '@/utils'

export default {
  namespaced: true,
  state: {
    id: 0,
    name: '',
    base: {},
    info: {},
    org: {},
    notice: [],     // 用户通知
    todo: []        // 用户待办
  },
  mutations: {
    updateUser (state, user) {
      state.id = user.id
      state.name = user.realName
    },
    updateId (state, id) {
      state.id = id
    },
    updateName (state, name) {
      state.name = name
    },
    updateBase (state, base) {
      state.base = base
    },
    updateInfo (state, info) {
      state.info = info
    },
    updateOrg (state, org) {
      state.org = org
    },
    updateNotice (state, data) {
      if (data.list && data.list.length > 0) state.notice = data.list
    },
    updateTodo (state, data) {
      if (data.list && data.list.length > 0) state.todo = data.list
    }
  },
  getters: {
    userId: (state) => {
      return state.id
    },
    userOrg: (state) => {
      return state.org
    },
    userNotice: (state) => {
      return state.notice
    },
    userTodo: (state) => {
      return state.todo
    }
  },
  actions: {
    loadNotice (context, category) {
      return doGet('/nm/notice/page', { page: 1, limit: 5, category: category}, {loading: false}).then(({data}) => {
        if (category === 1) context.commit('updateNotice', data)
        if (category === 2) context.commit('updateTodo', data)
      })
    }
  }
}
