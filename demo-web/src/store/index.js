import Vue from 'vue'
import Vuex from 'vuex'
import cloneDeep from 'lodash/cloneDeep'
import common from './modules/common'
import user from './modules/user'
import data from './modules/data'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    common,
    user,
    data
  },
  mutations: {
    // 重置vuex本地储存状态
    resetStore (state, namespaced) {
      Object.keys(state).forEach((key) => {
        if (namespaced) {
          if (namespaced.indexOf(key) >= 0) {
            state[key] = cloneDeep(window.SITE_CONFIG['storeState'][key])
          }
        } else {
          state[key] = cloneDeep(window.SITE_CONFIG['storeState'][key])
        }
      })
    }
  },
  strict: process.env.NODE_ENV !== 'production'
})
