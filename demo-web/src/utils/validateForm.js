import {isEmail, isMobile, isMoney, isNumber, isPositiveInteger, normalPassword} from '@/utils/validate'

/**
 * 表单验证器 数值
 */
export function numberValidator (rule, value, callback) {
  if (value != null && value !== '') {
    if (!isNumber(value)) {
      callback(new Error('请输入正确的数值'))
    } else {
      callback()
    }
  } else {
    callback()
  }
}

/**
 * 表单验证器 手机号码
 */
export function mobileValidator (rule, value, callback) {
  if (value && !isMobile(value)) {
    callback(new Error('手机号格式错误'))
  } else {
    callback()
  }
}

/**
 * 表单验证器 邮箱
 */
export function emailValidator (rule, value, callback) {
  if (value && !isEmail(value)) {
    callback(new Error('邮箱格式错误'))
  } else {
    callback()
  }
}

/**
 *  保留两位小数
 **/
export function moneyValidator (rule, value, callback) {
  if (value != null && value !== '') {
    if (!isMoney(value)) {
      callback(new Error('请输入正确的金额'))
    } else {
      callback()
    }
  } else {
    callback()
  }
}

export function quantityValidator (rule, value, callback) {
  if (value != null && value !== '') {
    if (!isPositiveInteger(value)) {
      callback(new Error('请输入正确的数量'))
    } else {
      callback()
    }
  } else {
    callback()
  }
}

export function passwordValidator (rule, value, callback) {
  if (value != null && value !== '') {
    if (!normalPassword(value)) {
      callback(new Error('密码必须为6-20位字母、数字或英文符号中任意两种'))
    } else {
      callback()
    }
  } else {
    callback()
  }
}
