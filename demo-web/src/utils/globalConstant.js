/**
 * 定义vue全局常量
 * @param Vue
 * @param options
 */
const install = (Vue, options) => {
  // 默认分页元素数量
  Vue.prototype.$defaultPageSize = 15
  // 默认分页元素数量选项
  Vue.prototype.$defaultPageSizeArray = [5, 10, 15, 30, 50, 100]

  // 统一dialog宽度
  Vue.prototype.$smallDialogWith = '500px'
  Vue.prototype.$mediumDialogWith = '900px'
  Vue.prototype.$largeDialogWith = '1200px'

  // 列间隔参数
  Vue.prototype.$rowGutter = 16
}

export default install
