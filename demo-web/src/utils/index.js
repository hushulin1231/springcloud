import Vue from 'vue'
import router from '@/router'
import store from '@/store'
import http from '@/utils/httpRequest'
import {Message} from 'element-ui'

/**
 * POST请求
 */
export function doPost (url, data, config) {
  return doHttp(url, 'post', data, config)
}

/**
 * GET请求
 */
export function doGet (url, data, config) {
  return doHttp(url, 'get', data, config)
}

/**
 * Http请求
 * @param url   请求地址
 * @param method 请求类型
 * @param data  请求参数
 * @param config 配置参数（loading：是否改变全局加载状态，默认true、message：是否显示自动处理消息，默认true）
 */
export async function doHttp (url, method, data, config) {
  // await sleep(1)
  return new Promise((resolve, reject) => {
    // console.log('url', url, 'method', method, 'data', data, 'config', config)
    // 校验数据有效性
    if (!(typeof (url) === 'string' && url)) {
      Message.error('请求参数不正确：请求URL不能为空且必须为字符串类型')
      return
    }
    if (!(typeof (method) === 'string' && method)) {
      Message.error('请求参数不正确：请求类型不能为空且必须为字符串类型')
      return
    }
    // 配置参数
    config = {loading: true, message: true, ...config}
    // 更改全局加载状态
    if (config.loading) store.commit('common/updateGlobalLoading', true)
    // 是否GET请求
    const isGetMethod = method === 'get' || method === 'GET'
    // 请求体
    const req = {}
    req.method = method
    req.url = http.adornUrl(url)
    req[isGetMethod ? 'params' : 'data'] = isGetMethod ? http.adornParams(data) : http.adornData(data)
    // 发送请求
    http(req).then(res => {
      const data = res.data
      if (data) {
        if (data.code === 0) {
         // if (!isGetMethod && config.message) Message.success('操作成功') //操作提示
          resolve(data)
        } else if (data.code === 401) {
          console.error(data.msg)
          reject(res)
        } else {
          Message.error(data.msg)
          reject(res)
        }
      } else {
        Message.error('无有效的响应数据')
        reject(res)
      }
    }).catch(error => {
      console.error('请求错误！', error)
      if (error.response) {
        Message.error(`[${error.response.status}]请求发生错误，${error.response.statusText}`)
      } else {
        Message.error(`请求超时，请检查网络后重试`)
      }
      reject(error)
    }).finally(() => {
      // 更改全局加载状态
      if (config.loading) store.commit('common/updateGlobalLoading', false)
    })
  })
}

/**
 * 根据文件id获取文件url
 * @param id 文件id
 */
export function getFileUrl (id) {
  return id && http.adornUrl(`/sys/oss/download/${id}?token=${Vue.cookie.get('token')}`)
}

/**
 * 获取uuid
 */
export function getUUID () {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    return (c === 'x' ? (Math.random() * 16 | 0) : ('r&0x3' | '0x8')).toString(16)
  })
}

/**
 * 是否有权限
 * @param {*} key
 */
export function isAuth (key) {
  const permissions = JSON.parse(sessionStorage.getItem('permissions') || '[]')
  if (key instanceof Array) {
    return key.some(item => permissions.indexOf(item) !== -1)
  }
  return permissions.indexOf(key) !== -1 || false
}

/**
 * 获取数据字典列表
 * @param dictCode 字典类型编码
 * @param showDel 显示已删除数据
 */
export function getDictList (dictCode, showDel = false) {
  if (typeof dictCode !== 'string') {
    console.error('获取数据字典列表出错：传入参数[dictCode]有误')
    return null
  }
  const dict = store.state.data.dict
  if (!dict) {
    console.error('获取数据字典列表出错：数据字典档案为空')
    return null
  }
  for (let i in dict) {
    if (dict[i].dictCode === dictCode) {
      if (showDel) {
        return dict[i].valList
      } else {
        return dict[i].valList.filter(item => item.isDel === 0)
      }
    }
  }

  return null
}

/**
 * 获取数据字典项
 * @param dictCode 字典类型编码
 * @param valId 字典内容id
 * @param showDel 显示已删除数据
 * @return 数据字典项
 */
export function getDict (dictCode, valId, showDel = true) {
  valId = Number(valId)
  if (typeof valId !== 'number') {
    // console.error('获取数据字典列表出错：传入参数[valId]有误')
    return {}
  }
  const list = getDictList(dictCode, showDel)
  if (!list) {
    console.warn(`获取数据字典项出错：数据字典[${dictCode}]列表为空`)
    return {}
  }
  for (let i in list) {
    if (list[i].id === valId) {
      return list[i]
    }
  }

  return {}
}

/**
 * 获取当前对象id和子集的id的集合
 * @param obj
 * @returns {[*]}
 */
export function getCascadeIds (obj) {
  let ids = [obj.id]
  if (obj.children && obj.children.length > 0) {
    for (const o of obj.children) {
      ids = ids.concat(getCascadeIds(o))
    }
  }
  return ids
}

/**
 * 获取当前对象id和父级的id的集合
 * @param node tree的node
 * @returns {[*]}
 */
export function getCascadeParentIds (node) {
  let ids = [node.data.id]
  if (node.parent !== null && node.parent.level > 0) {
    ids = ids.concat([node.parent.data.id])
  }
  if (node.parent.parent !== null) {
    return ids.concat(getCascadeParentIds(node.parent.parent))
  }
  return ids
}

/**
 * 树形数据转换
 * @param source 原平行数据
 * @param id id字段名称，默认为id
 * @param parentId 父id字段名称，默认为pid
 * @param children 子节点字段名称，默认为children
 * @returns {*} 树形数据
 */
export function treeDataTranslate (source, id = 'id', parentId = 'pid', children = 'children') {
  // 对源数据深度克隆
  let cloneData = JSON.parse(JSON.stringify(source))

  return cloneData.filter(father => { // 循环所有项，并添加children属性
    let branchArr = cloneData.filter(child => father[id] === child[parentId]) // 返回每一项的子级数组
    // father[children] = branchArr || '' // 给父级添加一个children属性，并赋值
    if (branchArr.length > 0) {
      father.children = branchArr
      father.leaf = false
    } else {
      father.leaf = true
    }
    return !father[parentId]        // 如果为根节点则返回false
  })
}

/**
 * 树形数据根据当前id获取当前节点对象
 * @param source 树形数据
 * @param id id
 * @param idField id字段名称，默认为id
 * @param parentIdField 父id字段名称，默认为pid
 * @param childrenField 子节点字段名称，默认为children
 * @returns {*} 当前节点对象
 */
export function treeDataFindById (source, id, idField = 'id', parentIdField = 'pid', childrenField = 'children') {
  // 对源数据深度克隆
  let cloneData = JSON.parse(JSON.stringify(source))

  let treeNode = {}
  if (cloneData.length === 0) {
    return treeNode
  }

  const rev = (data, nodeId) => {
    for (let i in data) {
      let node = data[i]
      if (node[idField] === nodeId) {
        treeNode = node
        return treeNode
      } else {
        if (node[childrenField]) {
          rev(node[childrenField], nodeId)
        }
      }
    }
    return treeNode
  }
  treeNode = rev(cloneData, id)
  return treeNode
}

/**
 * 树形数据根据当前id获取当前数据的子级id集合
 * @param node 树形数据当前节点对象
 * @param idField id字段名称，默认为id
 * @param childrenField 子节点字段名称，默认为children
 * @returns {*} 子级id集合
 */
export function treeDataFindChildren (node, idField = 'id', childrenField = 'children') {
  let children = [node[idField]]
  if (node[childrenField] && node[childrenField].length > 0) {
    for (const o of node[childrenField]) {
      children = children.concat(treeDataFindChildren(o, idField, childrenField))
    }
  }
  return children
}

/**
 * 树形数据根据当前id获取当前数据的上级id集合（由上到下排序）
 * @param source 树形数据
 * @param id id
 * @param idField id字段名称，默认为id
 * @param parentIdField 父id字段名称，默认为pid
 * @param childrenField 子节点字段名称，默认为children
 * @returns {*} 父级对象数组
 */
export function treeDataFindParents (source, id, idField = 'id', parentIdField = 'pid', childrenField = 'children') {
  // 对源数据深度克隆
  let cloneData = JSON.parse(JSON.stringify(source))

  var arrRes = []
  if (cloneData.length === 0) {
    return arrRes
  }
  const rev = (data, nodeId) => {
    for (let i in data) {
      let node = data[i]
      if (node[idField] === nodeId) {
        arrRes.unshift(node)
        rev(cloneData, node[parentIdField])
        break
      } else {
        if (node[childrenField]) {
          rev(node[childrenField], nodeId)
        }
      }
    }
    return arrRes
  }
  arrRes = rev(cloneData, id)
  return arrRes
}

/**
 * 树形数据根据当前id获取当前数据的最上级数据
 * @param source 树形数据
 * @param id id
 * @param idField id字段名称，默认为id
 * @param parentIdField 父id字段名称，默认为pid
 * @param childrenField 子节点字段名称，默认为children
 * @returns {*} 顶层对象
 */
export function treeDataFindTop (source, id, idField = 'id', parentIdField = 'pid', childrenField = 'children') {
  const arrRes = treeDataFindParents(source, id, idField, parentIdField, childrenField)
  return arrRes && arrRes[0]
}

/**
 * 清除登录信息
 */
export function clearLoginInfo () {
  Vue.cookie.delete('token')
  store.commit('resetStore', ['user', 'common'])
  router.options.isAddDynamicMenuRoutes = false
}

/**
 * 检查数组是否包含某元素
 * @param val       某元素
 * @param array     数组
 * @returns {boolean}
 */
export function inArray (val, array) {
  for (const it of array) {
    if (val === it) {
      return true
    }
  }
  return false
}

/**
 * 对象转value-name数组
 * @param obj 对象
 * @returns {boolean}
 */
export function toArray (obj) {
  if (typeof obj === 'object') {
    const array = Object.keys(obj).map(key => {
      try {
        key = Number(key)
      } catch (e) {}
      return {value: key, name: obj[key]}
    })
    return array
  }
  return null
}

/**
 * 提取url参数转为参数对象
 * @param str
 */
export function urlToObj (str) {
  var obj = {}
  try {
    var arr1 = str.split('?')
    var arr2 = arr1[1].split('&')
    for (let i = 0; i < arr2.length; i++) {
      var res = arr2[i].split('=')
      obj[res[0]] = res[1]
    }
  } catch (e) {
    console.error('Function urlToObj Error.', e)
  }
  return obj
}

/**
 * 对象转url参数
 * @param {*} data Object对象
 * @param {*} isPrefix 添加&符前缀， 默认false
 */
export function queryParams (data, isPrefix = false) {
  let prefix = isPrefix ? '?' : ''
  let _result = []
  for (let i in data) {
    let value = data[i]
    // 去掉为空的参数
    if (['', undefined, null].includes(value)) {
      continue
    }
    if (value.constructor === Array) {
      value.forEach(_value => {
        _result.push(encodeURIComponent(i) + '[]=' + encodeURIComponent(_value))
      })
    } else {
      _result.push(encodeURIComponent(i) + '=' + encodeURIComponent(value))
    }
  }

  return _result.length ? prefix + _result.join('&') : ''
}

/**
 * 等待ms毫秒后继续执行
 * @param ms 毫秒数
 * @return Promise
 */
export function sleep (ms) {
  return new Promise(function (resolve, reject) {
    setTimeout(resolve, ms)
  })
}

/**
 * 获取枚举集合
 * @param enumsName 枚举类型
 */
export function getEnums(enumsName) {
  return store.state.data.enums[enumsName]
}

/**
 * 通过枚举值获取枚举对象
 * @param enumsName 枚举类型
 * @param value 枚举值
 */
export function getEnum(enumsName, value) {
  const enums = getEnums(enumsName) // 获取枚举集合
  let enumObj = {} // 定义枚举对象
  enums && enums.some(item => {
    if (item.value === value) {
      enumObj = item
      return true
    }
    return false
  })
  return enumObj
}

/**
 * 通过枚举值获取名称
 * @param enumsName 枚举类型
 * @param value 枚举值
 */
export function getEnumLabel(enumsName, value) {
  const enumObj = getEnum(enumsName, value)
  return enumObj && enumObj.label
}
