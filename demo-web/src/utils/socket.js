// eslint-disable-next-line import/no-duplicates
import SockJS from 'sockjs-client'
import Stomp from 'stompjs'

const install = (Vue, options) => {
  Vue.prototype.$stompClient = null
  Vue.prototype.$stompTimer = null

  Vue.prototype.$initWebSocket = (uid) => {
    connection(uid)
    // 断开重连机制,尝试发送消息,捕获异常发生时重连
    // Vue.prototype.timer = setInterval(() => {
    //   try {
    //     Vue.prototype.$stompClient.send('test')
    //   } catch (err) {
    //     connection(uid)
    //   }
    // }, 10000)
  }

  function connection (uid) {
    const baseUrl = options.baseUrl
    // 建立连接对象
    let socket = new SockJS(`${baseUrl}/socket`)
    // 获取STOMP子协议的客户端对象
    const stompClient = Stomp.over(socket)
    stompClient.debug = null
    // 定义客户端的认证信息,按需求配置
    let headers = { Authorization: uid }
    // 向服务器发起websocket连接
    console.info('Connecting socket...')
    stompClient.connect(headers, (e) => {
      console.info('Connected success...')
      // 订阅服务端提供的某个topic
      stompClient.subscribe('/topic/notice', (msg) => {
        console.info('Subscribe /topic/notice', msg)
      }, headers)
      stompClient.subscribe(`/user/${uid}/notice`, (msg) => {
        console.info(`Subscribe /user/${uid}/notice`, msg)
      }, headers)
    }, (err) => {
      // 连接发生错误
      console.log('Socket connect error:', err)
    })
    Vue.prototype.$stompClient = stompClient
  }

  Vue.prototype.$closeWebSocket = (uid) => {
    if (Vue.prototype.$stompClient !== null) {
      Vue.prototype.$stompClient.disconnect({ Authorization: uid })
      if (Vue.prototype.$stompTimer !== null) {
        clearInterval(Vue.prototype.$stompTimer)
      }
    }
  }
}

export default install
