import {mobileValidator, moneyValidator, numberValidator, passwordValidator, quantityValidator} from './validateForm'
import {normalPassword} from "@/utils/validate";

/**
 * 定义vue全局Form验证函数
 */
const install = (Vue) => {
  Vue.prototype.FormRules = function (item) {
    const rules = []
    let trigger = 'blur'
    if (item.trigger) trigger = item.trigger
    if (item.required) {
      let ro = { required: true, message: '此项不能为空', trigger: trigger }
      if (item.message) {
        ro.message = item.message
      }
      rules.push(ro)
    }
    if (item.max && !item.min) {
      let ro = { min: 0, max: item.max, message: '最多输入' + item.max + '个字符', trigger: trigger }
      if (item.message) {
        ro.message = item.message
      }
      rules.push(ro)
    }
    if (item.min && item.max) {
      let ro = { min: item.min, max: item.max, message: '字符长度在' + item.min + '至' + item.max + '之间', trigger: trigger }
      if (item.message) {
        ro.message = item.message
      }
      rules.push(ro)
    }
    if (item.type) {
      const type = item.type
      switch (type) {
        case 'number': // 数值
          // rules.push({ type: 'number', message: '请输入数值', trigger: trigger })
          rules.push({ validator: numberValidator, trigger: trigger })
          break
        case 'email': // 邮箱
          rules.push({ type: 'email', message: '请输入正确的邮箱地址', trigger: trigger })
          break
        case 'money': // 金额
          rules.push({ validator: moneyValidator, trigger: trigger })
          break
        case 'mobile': // 手机号
          rules.push({ validator: mobileValidator, trigger: trigger })
          break
        // case 'regex': // 非法字符
        //   rules.push({ validator: isValidateRegex, trigger: trigger })
        //   break
        case 'quantity': // 数量
          rules.push({ validator: quantityValidator, trigger: trigger })
          break
        case 'password': // 密码
          rules.push({ validator: passwordValidator, trigger: trigger })
          break
        case 'if': // 判断
          rules.push({ validator: (rule, value, callback) => {
            if (value != null && value !== '') {
              if (item.validator(value)) {
                callback(new Error(item.message))
                return
              }
            }
            callback()
          }, trigger: trigger })
          break
        default:
          rules.push({})
          break
      }
    } else if (item.validator) {
      rules.push({ validator: item.validator, trigger: trigger })
    }
    return rules
  }
}

export default install
