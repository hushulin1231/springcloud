/**
 * 验证是否为数字
 */
export function isNumber (v) {
  return /^[0-9]+.?[0-9]*$/.test(v)
}

/**
 * 验证数量
 */
export function isPositiveInteger (v) {
  return /^[0-9]+$/.test(v)
}

/**
 * 验证合法金额
 */
export function isMoney (v) {
  return /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/.test(v)
}

/**
 * 邮箱
 * @param {*} s
 */
export function isEmail (s) {
  return /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/.test(s)
}

/**
 * 手机号码
 * @param {*} s
 */
export function isMobile (s) {
  return /^1[0-9]{10}$/.test(s)
}

/**
 * 电话号码
 * @param {*} s
 */
export function isPhone (s) {
  return /^([0-9]{3,4}-)?[0-9]{7,8}$/.test(s)
}

/**
 * URL地址
 * @param {*} s
 */
export function isURL (s) {
  return /^http[s]?:\/\/.*/.test(s)
}

/**
 * 简单密码
 * 最短6位，最长16位 {6,16}
 * 可以包含小写大母 [a-z] 和大写字母 [A-Z]
 * 可以包含数字 [0-9]
 * 可以包含英文符号
 * @param s 校验字符
 */
export function easyPassword (s) {
  return /^[\w\x21-\x7f]{6,16}$/.test(s)
}

/**
 * 中等密码
 * 最短6位，最长20位 {6,20}
 * 可以包含小写大母 [a-z] 和大写字母 [A-Z]
 * 可以包含数字 [0-9]
 * 可以包含英文符号
 * 至少包含字母、数字、符号至少两种
 * @param s 校验字符
 */
export function normalPassword (s) {
  return /((?=.*\d)(?=.*\D)|(?=.*[a-zA-Z])(?=.*[^a-zA-Z]))(?!^.*[\u4E00-\u9FA5].*$)^\S{6,20}$/.test(s)
}
