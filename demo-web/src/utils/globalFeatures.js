import httpRequest from '@/utils/httpRequest'
import _ from 'lodash'
import {
  doGet,
  doHttp,
  doPost,
  getCascadeIds,
  getCascadeParentIds,
  getDict,
  getDictList,
  getFileUrl,
  inArray,
  isAuth,
  queryParams,
  toArray,
  treeDataTranslate,
  getEnums, getEnum, getEnumLabel
} from '@/utils'

/**
 * 定义vue全局函数
 * @param Vue
 * @param options
 */
const install = (Vue, options) => {
  // lodash
  Vue.prototype.$_ = _

  // ajax请求方法
  Vue.prototype.$http = httpRequest

  // Http请求
  Vue.prototype.$doHttp = doHttp

  // Get请求
  Vue.prototype.$doGet = doGet

  // Post请求
  Vue.prototype.$doPost = doPost

  // 资源权限验证
  Vue.prototype.$isAuth = isAuth

  // 根据文件id获取文件url
  Vue.prototype.$getFileUrl = getFileUrl

  // 获取数据字典列表
  Vue.prototype.$getDictList = getDictList

  // 获取数据字典项
  Vue.prototype.$getDict = getDict

  // 对象是否在数组中
  Vue.prototype.$inArray = inArray

  // 对象转value-name数组
  Vue.prototype.$toArray = toArray

  // 列表转树
  Vue.prototype.$treeDataTranslate = treeDataTranslate

  // 获取对象及其子集对象的id的集合
  Vue.prototype.$getCascadeIds = getCascadeIds

  // 获取treeNode对象及其子集父级的id的集合
  Vue.prototype.$getCascadeParentIds = getCascadeParentIds

  // 对象转URL参数
  Vue.prototype.$queryParams = queryParams

  /**
   * 获取枚举集合
   * @param enumsName 枚举类型
   */
  Vue.prototype.$getEnums = getEnums

  /**
   * 通过枚举值获取枚举对象
   * @param enumsName 枚举类型
   * @param value 枚举值
   */
  Vue.prototype.$getEnum = getEnum

  /**
   * 通过枚举值获取名称
   * @param enumsName 枚举类型
   * @param value 枚举值
   */
  Vue.prototype.$getEnumLabel = getEnumLabel
}

export default install
