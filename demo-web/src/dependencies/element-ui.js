/**
 * UI组件, 统一使用饿了么桌面端组件库(https://github.com/ElemeFE/element）
 *
 * 使用:
 *  1. 项目中需要的组件进行释放(解开注释)
 *
 * 注意:
 *  1. 打包只会包含释放(解开注释)的组件, 减少打包文件大小
 */

// fade/zoom 等动画
import 'element-ui/lib/theme-chalk/base.css'

import Vue from 'vue'
import {
  Alert,
  Aside,
  Autocomplete,
  Backtop,
  Badge,
  Breadcrumb,
  BreadcrumbItem,
  Button,
  ButtonGroup,
  Card,
  Carousel,
  CarouselItem,
  Cascader,
  Checkbox,
  CheckboxButton,
  CheckboxGroup,
  Col,
  Collapse,
  CollapseItem,
  ColorPicker,
  Container,
  DatePicker,
  Dialog,
  Divider,
  Drawer,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  Footer,
  Form,
  FormItem,
  Header,
  Icon,
  Image,
  Input,
  InputNumber,
  Link,
  Loading,
  Main,
  Menu,
  MenuItem,
  MenuItemGroup,
  Message,
  MessageBox,
  Notification,
  Option,
  OptionGroup,
  PageHeader,
  Pagination,
  Popover,
  Progress,
  Radio,
  RadioButton,
  RadioGroup,
  Rate,
  Row,
  Scrollbar,
  Select,
  Slider,
  Step,
  Steps,
  Submenu,
  Switch,
  Table,
  TableColumn,
  TabPane,
  Tabs,
  Tag,
  Timeline,
  TimelineItem,
  TimePicker,
  TimeSelect,
  Tooltip,
  Transfer,
  Tree,
  Upload
} from 'element-ui'

Vue.component('el-pagination', Pagination)
Vue.component('el-dialog', Dialog)
Vue.component('el-autocomplete', Autocomplete)
Vue.component('el-dropdown', Dropdown)
Vue.component('el-dropdown-menu', DropdownMenu)
Vue.component('el-dropdown-item', DropdownItem)
Vue.component('el-menu', Menu)
Vue.component('el-submenu', Submenu)
Vue.component('el-menu-item', MenuItem)
Vue.component('el-menu-item-group', MenuItemGroup)
Vue.component('el-input', Input)
Vue.component('el-input-number', InputNumber)
Vue.component('el-radio', Radio)
Vue.component('el-radio-group', RadioGroup)
Vue.component('el-radio-button', RadioButton)
Vue.component('el-checkbox', Checkbox)
Vue.component('el-checkbox-button', CheckboxButton)
Vue.component('el-checkbox-group', CheckboxGroup)
Vue.component('el-scrollbar', Scrollbar)
Vue.component('el-switch', Switch)
Vue.component('el-select', Select)
Vue.component('el-option', Option)
Vue.component('el-option-group', OptionGroup)
Vue.component('el-button', Button)
Vue.component('el-button-group', ButtonGroup)
Vue.component('el-table', Table)
Vue.component('el-table-column', TableColumn)
Vue.component('el-date-picker', DatePicker)
Vue.component('el-time-select', TimeSelect)
Vue.component('el-time-picker', TimePicker)
Vue.component('el-popover', Popover)
Vue.component('el-tooltip', Tooltip)
Vue.component('el-breadcrumb', Breadcrumb)
Vue.component('el-breadcrumb-item', BreadcrumbItem)
Vue.component('el-form', Form)
Vue.component('el-form-item', FormItem)
Vue.component('el-tabs', Tabs)
Vue.component('el-tab-pane', TabPane)
Vue.component('el-tag', Tag)
Vue.component('el-tree', Tree)
Vue.component('el-alert', Alert)
Vue.component('el-slider', Slider)
Vue.component('el-icon', Icon)
Vue.component('el-row', Row)
Vue.component('el-col', Col)
Vue.component('el-upload', Upload)
Vue.component('el-progress', Progress)
Vue.component('el-badge', Badge)
Vue.component('el-card', Card)
Vue.component('el-rate', Rate)
Vue.component('el-steps', Steps)
Vue.component('el-step', Step)
Vue.component('el-carousel', Carousel)
Vue.component('el-carousel-item', CarouselItem)
Vue.component('el-collapse', Collapse)
Vue.component('el-collapse-item', CollapseItem)
Vue.component('el-cascader', Cascader)
Vue.component('el-color-picker', ColorPicker)
Vue.component('el-transfer', Transfer)
Vue.component('el-container', Container)
Vue.component('el-header', Header)
Vue.component('el-aside', Aside)
Vue.component('el-main', Main)
Vue.component('el-footer', Footer)
Vue.component('el-divider', Divider)
Vue.component('el-link', Link)
Vue.component('el-timeline', Timeline)
Vue.component('el-timeline-item', TimelineItem)
Vue.component('el-page-header', PageHeader)
Vue.component('el-image', Image)
Vue.component('el-backtop', Backtop)
Vue.component('el-drawer', Drawer)

Vue.use(Loading.directive)

Vue.prototype.$loading = Loading.service
Vue.prototype.$msgbox = MessageBox
Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$prompt = MessageBox.prompt
Vue.prototype.$notify = Notification
Vue.prototype.$message = Message

Vue.prototype.$ELEMENT = { size: 'medium' }
