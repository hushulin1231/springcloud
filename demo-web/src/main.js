import Vue from "vue";
import App from "@/App";
import router from "@/router";
import store from "@/store";
import VueCookie from "vue-cookie";
import Viser from "viser-vue"; // Viser图表
import VueUeditorWrap from "vue-ueditor-wrap";
// import '@/dependencies/element-ui' // element-ui 按需引入
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import locale from "element-ui/lib/locale/lang/zh-CN";
import "@/icons"; // svg icons
import "@/assets/scss/index.scss"; // 自定义样式文件引入
import cloneDeep from "lodash/cloneDeep"; // lodash 克隆
import init from "@/utils/init"; // 初始化
import GlobalFeatures from "@/utils/globalFeatures"; // 全局方法引入
import GlobalConstant from "@/utils/globalConstant"; // 全局常量引入
import GlobalFormValid from "@/utils/globalFormValid"; // 全局表单验证
// 自定义组件引入
import DictSelect from "@/components/select/dict-select";
import EnumsSelect from "@/components/select/enums-select";
import UrlSelect from "@/components/select/url-select";
import TreeSelect from "@/components/select/tree-select";
import TreeTableSelect from "@/components/select/tree-table-select";
import TableSelect from "@/components/select/table-select";
import ImageUpload from "@/components/upload/image-upload";
import FileUpload from "@/components/upload/file-upload";
import ProcessView from "@/components/process/process-view";
import ProcessAction from "@/components/process/process-action";
import UserInfo from "@/components/user-info";
import Pagination from "@/components/pagination";
import EditTable from "@/components/edit-table";
import MultiTree from "@/components/multi-tree";

//富文本域

import VueQuillEditor  from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
 
Vue.use(VueQuillEditor);
// socket
import Socket from "@/utils/socket";

import "@/utils/directive";

require("@/icons/index");

Vue.use(ElementUI, { size: "small", locale: locale });

Vue.use(Viser);

Vue.use(VueCookie);

Vue.config.productionTip = false;

// 挂载全局
Vue.use(init); // 初始化
Vue.use(GlobalFeatures); // 全局方法
Vue.use(GlobalConstant); // 全局常量
Vue.use(GlobalFormValid); // 全局表单校验
Vue.component("dict-select", DictSelect); // 字典选择
Vue.component("enums-select", EnumsSelect); // 枚举选择
Vue.component("url-select", UrlSelect); // 链接选择
Vue.component("tree-select", TreeSelect); // 树选择
Vue.component("tree-table-select", TreeTableSelect); // 左树右表选择
Vue.component("table-select", TableSelect);
Vue.component("image-upload", ImageUpload); // 图片上传
Vue.component("file-upload", FileUpload); // 文件上传
Vue.component("process-view", ProcessView); // 流程记录查看
Vue.component("process-action", ProcessAction); // 流程处理
Vue.component("user-info", UserInfo); // 用户信息
Vue.component("pagination", Pagination); // 分页
Vue.component("vue-ueditor-wrap", VueUeditorWrap); // Ueditor
Vue.component("edit-table", EditTable); // 子表表格
Vue.component("multi-tree", MultiTree); // 多选树

Vue.prototype.$cloneDeep = cloneDeep;

// 保存整站vuex本地储存初始状态
window.SITE_CONFIG["storeState"] = cloneDeep(store.state);
window.SITE_CONFIG["storeState"].common.globalLoading = false;

Vue.use(Socket, { baseUrl: window.SITE_CONFIG.baseUrl });

// 根据路由设置标题
router.beforeEach((to, from, next) => {
  /* 路由发生改变修改页面的title */
  if (to.meta.title) {
    document.title = `${to.meta.title} - ${store.state.data.logoConfig.title}`;
  }
  next();
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
