const path = require('path');
function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  devServer: {
    port: 8000,     // 端口
    proxy: {  // 代理
      '/cg-api': {
        target: 'http://localhost:8002',
        changeOrigin: true,
        pathRewrite: {
          '^/cg-api': '/'
        }
      }
    }
  },
  publicPath: './',
  productionSourceMap: process.env.NODE_ENV !== 'production',
  chainWebpack: config => {
    config.resolve.alias
        .set('@', resolve('src'))

    config.module
      .rule('svg')
      .exclude.add(resolve('src/icons'))
      .end()

    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: '[name]'
      })
  }
};