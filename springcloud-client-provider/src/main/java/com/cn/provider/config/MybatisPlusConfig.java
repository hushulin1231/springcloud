package com.cn.provider.config;

import com.baomidou.mybatisplus.core.parser.ISqlParser;
import com.baomidou.mybatisplus.core.parser.ISqlParserFilter;
import com.baomidou.mybatisplus.extension.parsers.BlockAttackSqlParser;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * mybatis-plus配置

 */
@Configuration
public class MybatisPlusConfig {

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();

        List<ISqlParser> sqlParserList = new ArrayList<>();

        // 攻击 SQL 阻断解析器、加入解析链 （作用！阻止恶意的全表更新删除）
        sqlParserList.add(new BlockAttackSqlParser());

        // sql条件过滤器
        paginationInterceptor.setSqlParserFilter(new ISqlParserFilter() {
            @Override
            public boolean doFilter(MetaObject metaObject) {
                // 判断是否过滤，true表示过滤掉当前sql的条件处理器
                return true;
            }
        });

        // 添加sql条件
        paginationInterceptor.setSqlParserList(sqlParserList);

        return paginationInterceptor;
    }

}
