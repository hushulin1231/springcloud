package com.cn.provider.config;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.LFUCache;
import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.cn.provider.modules.sys.entity.SysMenuEntity;
import com.cn.provider.modules.sys.entity.SysUserEntity;
import com.cn.provider.modules.sys.service.SysMenuService;
import com.cn.provider.modules.sys.service.SysUserService;




/**
 * 缓存配置
 */
@Configuration
@Slf4j
public class CacheConfig {

    @Autowired
    private  SysUserService sysUserService;
    
	
	 @Autowired 
	 private SysMenuService sysMenuService;
	 
    
    /**
     * 用户列表缓存，参数0, 0表示数量和有效时间无限制
     */
    public static LFUCache<Long, SysUserEntity> userCache = CacheUtil.newLFUCache(0, 0);


    /**
     * 菜单列表缓存，参数0, 0表示数量和有效时间无限制
     */
    public static LFUCache<Long, SysMenuEntity> menuCache = CacheUtil.newLFUCache(0, 0);

    /**
     * 缓存初始化
     */
	
	 @Bean
	 public void cacheInit() {
		 this.createCache();
	 }
	 /**
	     * 创建缓存数据
	     */
	    public void createCache() {
	        log.info("开始创建数据缓存......");
	        long start = DateUtil.current(false);

	        //创建用户缓存
	        List<SysUserEntity> users = sysUserService.list();
	        userCache.clear();
	        users.forEach(user -> userCache.put((long) user.getId(), user));

		
			//创建菜单缓存 
			List<SysMenuEntity> menus = sysMenuService.listAll();
			menuCache.clear(); 
			menus.forEach(menu -> menuCache.put(Long.valueOf(menu.getId()), menu));


	        long end = DateUtil.current(false);
	        long time = end - start;
	        log.info("创建数据缓存完成，耗时{}毫秒", time);
	    }

	    /**
	     * 获取菜单缓存
	     * @param menuId 菜单id
	     * @return 菜单实体
	     */
	    public static SysMenuEntity getMenuCache(Long menuId) {
	        SysMenuEntity menu = null;
	        try {
	            if (menuId != null) {
	                menu = CacheConfig.menuCache.get(menuId);
	            }
	        } catch (Exception e) {
	            log.debug("获取菜单缓存出错：缓存对象不存在", e);
	        }

	        return menu == null ? new SysMenuEntity() : menu;
	    }

}
