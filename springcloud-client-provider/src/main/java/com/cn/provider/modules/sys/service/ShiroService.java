package com.cn.provider.modules.sys.service;


import java.util.Set;

import com.cn.provider.modules.sys.entity.SysUserEntity;
import com.cn.provider.modules.sys.form.SysLoginForm;
import com.cn.provider.modules.sys.form.SysMenuQueryForm;

public interface ShiroService {

    Set<String> getUserPermissions(SysMenuQueryForm queryForm);
    
    /**
     * 用户登录
     * @param form
     * @return
     */
    SysUserEntity login(SysLoginForm form);
}
