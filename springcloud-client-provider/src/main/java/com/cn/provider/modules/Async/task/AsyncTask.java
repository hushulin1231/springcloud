package com.cn.provider.modules.Async.task;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@EnableAsync
public class AsyncTask {

  @Async
  public void register() {
	 log.info("多线程开始注册模拟");
	 try {
		  Thread.sleep(1000*1);
	  } catch (Exception e) {
		e.printStackTrace();
	 }
	  log.info("多线程注册成功");
  }
  

}
