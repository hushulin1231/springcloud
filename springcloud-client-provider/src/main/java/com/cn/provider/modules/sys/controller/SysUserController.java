package com.cn.provider.modules.sys.controller;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cn.provider.common.annotation.SysLog;
import com.cn.provider.common.utils.Constant;
import com.cn.provider.common.utils.PageInfo;
import com.cn.provider.common.utils.R;
import com.cn.provider.common.utils.ShiroUtils;
import com.cn.provider.modules.sys.form.SysUserQueryForm;
import com.cn.provider.modules.sys.form.SysUserUpdateBatchForm;
import com.cn.provider.modules.sys.service.SysUserRoleService;
import com.cn.provider.modules.sys.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.cn.provider.modules.sys.entity.SysUserEntity;


@Api(tags={"用户信息相关"})
@RestController
@RequestMapping("/sys/user")
public class SysUserController {
	
	@Autowired
	private  SysUserService sysUserService;
	
	@Autowired
	private  SysUserRoleService sysUserRoleService;
	
	/**
	 * 分页查询用户信息
	 * @param params
	 * @return
	 */
	@ApiOperation(value="分页查询用户信息")
	@GetMapping("/page")
	@SysLog("查询用户信息")
	public R<PageInfo<SysUserEntity>> page(SysUserQueryForm params){

		PageInfo<SysUserEntity> page = sysUserService.queryPage(params);

		return R.ok(page);
	}
	
	/**
	 * 保存用户
	 */
	@ApiOperation(value="新增用户")
	@SysLog("新增用户")
	@PostMapping("/save")
	public R<?> save(@RequestBody SysUserEntity user){
		user.setCreator(ShiroUtils.getUserId());
		user.setCreateTime(new Date());
		sysUserService.saveUser(user);
		return R.ok();
	}
	
	
	 /**
     * 根据ID查询用户详细信息
     * @param userId
     * @return
     */
	@ApiOperation(value="根据ID查询用户详细信息")
	@GetMapping("/info/{userId}")
	@SysLog("根据ID查询用户详细信息")
	public R<SysUserEntity> info(@PathVariable("userId") int userId){
		SysUserEntity user = sysUserService.getById(userId);
		user.setPassword(null);
		//获取用户所属的角色列表
		List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
		user.setRoleIdList(roleIdList);
		return R.ok(user);
	}
	
	
	/**
	 * 修改用户信息
	 */
	@ApiOperation(value="修改用户信息")
	@SysLog("修改用户")
	@PostMapping("/update")
	public R<?> update(@RequestBody SysUserEntity user){
		user.setUpdateTime(new Date());
		sysUserService.update(user);
		return R.ok();
	}
	
	/**
	 * 删除用户
	 */
	@ApiOperation(value="删除用户")
	@SysLog("删除用户")
	@PostMapping("/delete/{id}")
	public R<?> delete(@PathVariable("id") int id){
		if(id==Constant.SUPER_ADMIN) {
			return R.error("系统管理员不能删除");
		}
        if(id==ShiroUtils.getUserId()){
        	return R.error("当前用户不能删除");
        }
	    sysUserService.delete(id);
		return R.ok();
	}

	/**
	 * 重置密码
	 */
	@ApiOperation(value = "重置密码")
	@SysLog("重置密码")
	@PostMapping("/updateBatch")
	public R<?> updateBatch(@RequestBody SysUserUpdateBatchForm form){
		sysUserService.updateBatch(form.getUser(), form.getUserIds());

		return R.ok();
	}
	
}
