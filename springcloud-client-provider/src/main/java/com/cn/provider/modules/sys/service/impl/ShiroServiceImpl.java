package com.cn.provider.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cn.provider.common.utils.Constant;
import com.cn.provider.common.utils.Md5Util;
import com.cn.provider.common.utils.ShiroUtils;
import com.cn.provider.modules.sys.entity.SysMenuEntity;
import com.cn.provider.modules.sys.entity.SysUserEntity;
import com.cn.provider.modules.sys.form.SysLoginForm;
import com.cn.provider.modules.sys.form.SysMenuQueryForm;
import com.cn.provider.modules.sys.mapper.SysMenuMapper;
import com.cn.provider.modules.sys.mapper.SysUserMapper;
import com.cn.provider.modules.sys.service.ShiroService;
import com.cn.provider.modules.sys.service.SysUserRoleService;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.*;

@Service
public class ShiroServiceImpl implements ShiroService {

    @Resource
    private SysMenuMapper sysMenuMapper;

    @Resource
    private SysUserMapper userMapper;
    
    @Autowired
    private SysUserRoleService sysUserRoleService;
    
      
    /**
     * 登录
     */
    @Transactional(rollbackFor = Exception.class)
	@Override
	public SysUserEntity login(SysLoginForm form) {
		String username = form.getUsername();
        String password = Md5Util.md5(form.getPassword());
        AuthenticationToken token = new UsernamePasswordToken(username, password);
        SecurityUtils.getSubject().login(token);
        SysUserEntity user = (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
        //获取用户所属的角色列表
        List<Long> roleIdList = sysUserRoleService.queryRoleIdList(user.getId());
        user.setRoleIdList(roleIdList);
        return user;
	}
    
    
    
    /**
     * 查询用户的所有权限
     */
    @Override
    public Set<String> getUserPermissions(SysMenuQueryForm params) {
        List<String> permsList;
        int userId = ShiroUtils.getUserId();
        //系统管理员，拥有最高权限
        if(userId == Constant.SUPER_ADMIN){
            List<SysMenuEntity> menuList = sysMenuMapper.selectList(new LambdaQueryWrapper<SysMenuEntity>());
            permsList = new ArrayList<>(menuList.size());
            for(SysMenuEntity menu : menuList){
                permsList.add(menu.getPerms());
            }
        }else{
            permsList = userMapper.queryAllPerms(userId, params.getProp());
        }
        //用户权限列表
        Set<String> permsSet = new HashSet<>();
        for(String perms : permsList){
            if(StringUtils.isBlank(perms)){
                continue;
            }
            permsSet.addAll(Arrays.asList(perms.trim().split(",")));
        }
        return permsSet;
    }
    
   


}
