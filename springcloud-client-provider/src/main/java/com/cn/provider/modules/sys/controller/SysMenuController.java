package com.cn.provider.modules.sys.controller;

import java.util.List;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cn.provider.common.annotation.SysLog;
import com.cn.provider.common.exception.RRException;
import com.cn.provider.common.utils.Constant;
import com.cn.provider.common.utils.R;
import com.cn.provider.common.utils.ShiroUtils;
import com.cn.provider.modules.sys.entity.SysMenuEntity;
import com.cn.provider.modules.sys.form.SysMenuQueryForm;
import com.cn.provider.modules.sys.service.ShiroService;
import com.cn.provider.modules.sys.service.SysMenuService;
import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@Api(tags={"系统菜单"})
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController  {
	@Autowired
	private SysMenuService sysMenuService;
	
	@Autowired
	private ShiroService shiroService;

	/**
	 * 系统菜单导航
	 * 根据用户权限获取菜单
	 * @param params
	 * @return
	 */
	@ApiOperation(value="根据用户权限获取菜单")
	@SysLog("系统菜单导航")
	@GetMapping("/nav")
	public R<?> nav(SysMenuQueryForm params) {
		List<SysMenuEntity> menuList = sysMenuService.getUserMenuList(params);
		Set<String> permissions = shiroService.getUserPermissions(params);
		return R.ok().put("menuList", menuList).put("permissions", permissions);
	}


     /**
      * 获取菜单列表
      * @param params
      * @return
      */
	@ApiOperation(value="获取菜单列表")
	@SysLog("获取菜单列表")
	@GetMapping("/list")
	public R<List<SysMenuEntity>> list(SysMenuQueryForm params){
		LambdaQueryWrapper<SysMenuEntity> query = new LambdaQueryWrapper<>();
		query.orderByAsc(SysMenuEntity::getOrderNum);
		List<SysMenuEntity> menuList = sysMenuService.list(new LambdaQueryWrapper<SysMenuEntity>()
				.eq(SysMenuEntity::getProp, params.getProp())
				.inSql(ShiroUtils.getUserId() != Constant.SUPER_ADMIN, SysMenuEntity::getId, StrUtil.format("select rm.menu_id from sys_role_menu rm join sys_user_role ur on rm.role_id = ur.role_id where ur.user_id={}", ShiroUtils.getUserId()))
				.orderByAsc(SysMenuEntity::getOrderNum)
		);

		return R.ok(menuList);
	}


	/**
	 * 选择菜单(添加、修改菜单)
	 * 不包含资源权限的菜单列表
	 * @param params
	 * @return
	 */
	@ApiOperation(value="选择菜单(添加、修改菜单)")
	@SysLog("选择菜单(添加、修改菜单)")
	@GetMapping("/select")
	public R<List<SysMenuEntity>> select(SysMenuQueryForm params){
		//查询列表数据
		List<SysMenuEntity> menuList = sysMenuService.list(new LambdaQueryWrapper<SysMenuEntity>()
				.eq(SysMenuEntity::getProp, params.getProp())
				.ne(SysMenuEntity::getType, 2)
				.orderByAsc(SysMenuEntity::getOrderNum)
		);

		return R.ok(menuList);
	}


	/**
	 * 查询菜单详情
	 * @param menuId
	 * @return
	 */
	@ApiOperation(value="查询菜单详情")
	@SysLog("查询菜单详情")
	@GetMapping("/info/{menuId}")
	public R<SysMenuEntity> info(@PathVariable("menuId") Long menuId){
		SysMenuEntity menu = sysMenuService.getById(menuId);
		return R.ok(menu);
	}


    /**
     * 新增菜单
     * @param menu
     * @return
     */
	@ApiOperation(value="新增菜单")
	@SysLog("新增菜单")
	@PostMapping("/save")
	@RequiresPermissions("sys:menu:save")
	public R<?> save(@RequestBody SysMenuEntity menu){
		//数据校验
		verifyForm(menu);
		sysMenuService.save(menu);
		return R.ok();
	}

    /**
     * 修改菜单
     * @param menu
     * @return
     */
	@ApiOperation(value="修改菜单")
	@SysLog("修改菜单")
	@PostMapping("/update")
	@RequiresPermissions("sys:menu:update")
	public R<?> update(@RequestBody SysMenuEntity menu){
		//数据校验
		verifyForm(menu);
		sysMenuService.updateById(menu);
		return R.ok();
	}


	/**
	 * 删除菜单
	 * @param menuId
	 * @return
	 */
	@ApiOperation(value="删除菜单")
	@SysLog("删除菜单")
	@PostMapping("/delete/{menuId}")
	@RequiresPermissions("sys:menu:delete")
	public R<?> delete(@PathVariable("menuId") long menuId){
		if(menuId <= 31){
			return R.error("系统菜单，不能删除");
		}
		//判断是否有子菜单或按钮
		List<SysMenuEntity> menuList = sysMenuService.queryListParentId(menuId);
		if(menuList.size() > 0){
			return R.error("请先删除子菜单或按钮");
		}

		sysMenuService.delete(menuId);

		return R.ok();
	}

	/**
	 * 查询所有下级菜单
	 * @param parentId
	 * @return
	 */
	@ApiOperation(value="查询所有下级菜单")
	@SysLog("查询所有下级菜单")
	@GetMapping("/queryAllChildren")
	public R<List<SysMenuEntity>> queryAllChildren(@RequestParam @ApiParam(value = "父级id", required = true) int parentId){
		return R.ok(sysMenuService.queryAllChildren(parentId));
	}
	/**
	 * 验证参数是否正确
	 */
	private void verifyForm(SysMenuEntity menu){
		if(StringUtils.isBlank(menu.getName())){
			throw new RRException("菜单名称不能为空");
		}

		if(menu.getParentId() == 0){
			menu.setParentId(0);
		}

		//菜单
		if(menu.getType() == Constant.MenuType.MENU.getValue()){
			if(StringUtils.isBlank(menu.getUrl())){
				throw new RRException("菜单URL不能为空");
			}
		}

		//上级菜单类型
		int parentType = Constant.MenuType.CATALOG.getValue();
		if(menu.getParentId() != 0){
			SysMenuEntity parentMenu = sysMenuService.getById(menu.getParentId());
			parentType = parentMenu.getType();
		}

		//目录、菜单
		if(menu.getType() == Constant.MenuType.CATALOG.getValue() ||
				menu.getType() == Constant.MenuType.MENU.getValue()){
			if(parentType != Constant.MenuType.CATALOG.getValue()){
				throw new RRException("上级菜单只能为目录类型");
			}
			return ;
		}

		//按钮
		if(menu.getType() == Constant.MenuType.BUTTON.getValue()){
			if(parentType != Constant.MenuType.MENU.getValue()){
				throw new RRException("上级菜单只能为菜单类型");
			}
		}
	}



}
