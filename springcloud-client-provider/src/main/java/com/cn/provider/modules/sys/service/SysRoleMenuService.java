package com.cn.provider.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cn.provider.modules.sys.entity.SysRoleMenuEntity;

import java.util.List;



/**
 * 角色与菜单对应关系
 *
 * @author Mark sunlightcs@gmail.com
 */
public interface SysRoleMenuService extends IService<SysRoleMenuEntity> {

	/**
	 * 保存角色与菜单对应关系数据
	 * @param roleId 角色
	 * @param menuIdList 菜单id集合
	 */
	void saveOrUpdate(int roleId, List<Long> menuIdList);

	/**
	 * 根据角色ID，获取菜单ID列表
	 * @param roleId 角色id
	 * @return 菜单id集合
	 */
	List<Long> queryMenuIdList(Long roleId);

	/**
	 * 根据权限标识，获取有权限用户ID列表
	 * @param perms 权限标识数组
	 * @return 用户id集合
	 */
	List<Long> queryPermUserIdList(String[] perms);

	
	
    /**
     * 删除角色与菜单关联
     * @param roleIds
     */
	void deleteBatch(Long[] roleIds);

}
