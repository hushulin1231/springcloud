package com.cn.provider.modules.sys.service.impl;

import java.util.Arrays;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cn.provider.common.utils.PageUtils;
import com.cn.provider.common.utils.Query;
import com.cn.provider.modules.sys.entity.SysDictValEntity;
import com.cn.provider.modules.sys.form.SysDictValQueryForm;
import com.cn.provider.modules.sys.mapper.SysDictValMapper;
import com.cn.provider.modules.sys.service.SysDictValService;
import cn.hutool.core.util.StrUtil;


@Service
public class SysDictValServiceImpl extends ServiceImpl<SysDictValMapper, SysDictValEntity>implements SysDictValService{

	/**
	 * 分页查询数据字典内容
	 */
	@Override
	public PageUtils<SysDictValEntity> queryPage(SysDictValQueryForm params) {
		  IPage<SysDictValEntity> page = this.page(
	                new Query<SysDictValEntity>().getPage(params),
	                new LambdaQueryWrapper<SysDictValEntity>()
	                        .eq(SysDictValEntity::getDictId, params.getDictId())
	                        .and(StrUtil.isNotEmpty(params.getKey()), wapper -> wapper
	                                .like(SysDictValEntity::getValCode, params.getKey())
	                                .or()
	                                .like(SysDictValEntity::getValName, params.getKey())
	                        )
	                        .orderByAsc(SysDictValEntity::getValCode)
	        );
	        return new PageUtils<>(page);
	}

	/**
	 * 删除数据字典id对应的数据字典内容
	 */
	@Override
	public void deleteBatch(Long[] ids) {
		this.remove(new LambdaQueryWrapper<SysDictValEntity>().in(SysDictValEntity::getDictId, Arrays.asList(ids)));
		
	}


}
