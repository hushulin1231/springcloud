package com.cn.provider.modules.sys.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cn.provider.modules.sys.entity.SysMenuEntity;

@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenuEntity>{

}
