package com.cn.provider.modules.sys.service.impl;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cn.provider.common.utils.PageUtils;
import com.cn.provider.common.utils.Query;
import com.cn.provider.modules.sys.entity.SysDictEntity;
import com.cn.provider.modules.sys.form.SysDictQueryForm;
import com.cn.provider.modules.sys.mapper.SysDictMapper;
import com.cn.provider.modules.sys.service.SysDictService;
import com.cn.provider.modules.sys.service.SysDictValService;

import cn.hutool.core.util.StrUtil;

@Service
public class SysDictServiceImpl extends  ServiceImpl<SysDictMapper, SysDictEntity>  implements SysDictService{

	@Autowired
	private SysDictValService sysDictValService;
	/**
	 * 分页查询数据字典
	 */
	@Override
	public PageUtils<SysDictEntity> queryPage(SysDictQueryForm params) {
		  IPage<SysDictEntity> page = this.page(
	                new Query<SysDictEntity>().getPage(params),
	                new LambdaQueryWrapper<SysDictEntity>()
	                    .and(StrUtil.isNotBlank(params.getKey()), wapper -> wapper
	                            .like(SysDictEntity::getDictCode, params.getKey())
	                            .or()
	                            .like(SysDictEntity::getDictName, params.getKey()))
	                    .orderByAsc(SysDictEntity::getDictCode)
	        );
	        return new PageUtils<>(page);
	}

	/**
	 * 除数据字典
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delete(Long[] ids) {
		//删除数据字典id对应的数据字典内容
		sysDictValService.deleteBatch(ids);
		//删除数据字典
		this.removeByIds(Arrays.asList(ids));
		
	}

	

}
