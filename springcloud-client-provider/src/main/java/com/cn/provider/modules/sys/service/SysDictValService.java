package com.cn.provider.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cn.provider.common.utils.PageUtils;
import com.cn.provider.modules.sys.entity.SysDictValEntity;
import com.cn.provider.modules.sys.form.SysDictValQueryForm;

public interface SysDictValService extends IService<SysDictValEntity>{
    
	/**
	 * 分页查询数据字典内容
	 * @param params
	 * @return
	 */
	PageUtils<SysDictValEntity> queryPage(SysDictValQueryForm params);
    
	/**
	 * 删除数据字典id对应的数据字典内容
	 * @param ids
	 */
	void deleteBatch(Long[] ids);

}
