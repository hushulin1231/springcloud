package com.cn.provider.modules.sys.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cn.provider.modules.sys.entity.SysMenuEntity;
import com.cn.provider.modules.sys.form.SysMenuQueryForm;

public interface SysMenuService extends IService<SysMenuEntity>{
	/**
	 * 整表数据
	 */
	List<SysMenuEntity> listAll();

	/**
	 * 根据父菜单，查询子菜单
	 * @param parentId 父菜单ID
	 */
	List<SysMenuEntity> queryListParentId(Long parentId);

	/**
	 * 获取用户菜单列表
	 */
	List<SysMenuEntity> getUserMenuList(SysMenuQueryForm params);

	/**
	 * 删除
	 */
	void delete(Long menuId);

	/**
	 * 根据父级id获取所有子集
	 * @param parentId 父级id
	 * @return 所有子集
	 */
	List<SysMenuEntity> queryAllChildren(int parentId);

}
