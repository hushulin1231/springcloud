package com.cn.provider.modules.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cn.provider.common.utils.PageUtils;
import com.cn.provider.modules.sys.entity.SysRoleEntity;
import com.cn.provider.modules.sys.form.SysRoleQueryForm;



/**
 * 角色

 */
public interface SysRoleService extends IService<SysRoleEntity> {
    
	/**
	 * 分页查询角色列表
	 * @param params
	 * @return
	 */
	PageUtils<SysRoleEntity> queryPage(SysRoleQueryForm params);
	
	
    /**
    * 删除角色
    * @param roleIds
    */
	void deleteBatch(Long[] roleIds);
	
    /**
     * 修改角色
     * @param role
     */
    void update(SysRoleEntity role);


}
