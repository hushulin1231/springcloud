package com.cn.provider.modules.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cn.provider.common.utils.PageUtils;
import com.cn.provider.modules.sys.entity.SysLogEntity;
import com.cn.provider.modules.sys.form.SysDictQueryForm;


public interface SysLogService extends IService<SysLogEntity>{
	
	/**
	 * 分页查询日志信息
	 * @param params
	 * @return
	 */
	PageUtils<SysLogEntity> queryPage(SysDictQueryForm params);

}
