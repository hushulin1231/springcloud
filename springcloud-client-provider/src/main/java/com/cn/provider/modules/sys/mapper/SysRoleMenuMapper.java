package com.cn.provider.modules.sys.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cn.provider.modules.sys.entity.SysRoleMenuEntity;

@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenuEntity>{

	List<Long> queryMenuIdList(Long roleId);

	List<Long> queryPermUserIdList(QueryWrapper<Object> in);

}
