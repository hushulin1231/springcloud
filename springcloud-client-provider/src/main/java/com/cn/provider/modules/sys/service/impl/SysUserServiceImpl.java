package com.cn.provider.modules.sys.service.impl;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cn.provider.common.exception.RRException;
import com.cn.provider.common.utils.Md5Util;
import com.cn.provider.common.utils.PageInfo;
import com.cn.provider.config.CacheConfig;
import com.cn.provider.modules.sys.entity.SysMenuEntity;
import com.cn.provider.modules.sys.entity.SysUserEntity;
import com.cn.provider.modules.sys.entity.SysUserRoleEntity;
import com.cn.provider.modules.sys.form.SysUserQueryForm;
import com.cn.provider.modules.sys.mapper.SysUserMapper;
import com.cn.provider.modules.sys.service.SysUserRoleService;
import com.cn.provider.modules.sys.service.SysUserService;
import com.github.pagehelper.PageHelper;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;


@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUserEntity> implements SysUserService{
	
	@Autowired
	private SysUserMapper sysUserMapper;
	
	@Autowired
	private SysUserRoleService sysUserRoleService;
	
	/**
	 * 分页查询用户信息
	 */
	@Override
	public PageInfo<SysUserEntity> queryPage(SysUserQueryForm params) {
		  PageHelper.startPage(params.getPage().intValue(),params.getLimit().intValue());
	      List<SysUserEntity> page = sysUserMapper.page(params);
	      return new PageInfo<SysUserEntity>(page);
	}
	
    /**
     * 新增用户
     */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveUser(SysUserEntity user) {
		this.saveValid(user);
		//密码md5加密
		String password=Md5Util.md5(user.getPassword());
		user.setPassword(password);
        this.save(user);
		//保存用户与角色关系
		sysUserRoleService.saveRole(user.getId(), user.getRoleIdList());
		//更新缓存
		CacheConfig.userCache.put(Long.valueOf(user.getId()), user);
		
	}
	
	/**
	 * 修改用户信息
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(SysUserEntity user) {
	    user.setPassword(null);
		this.updateById(user);
		//保存用户与角色关系
		sysUserRoleService.updateRole(user.getId(), user.getRoleIdList());
		//更新缓存
		CacheConfig.userCache.put(Long.valueOf(user.getId()), user);
		
	}
	
	/**
	 * 删除用户信息
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delete(int id) {
		//通过用户id删除用户角色关联表信息	
		sysUserRoleService.remove(new LambdaQueryWrapper<SysUserRoleEntity>().eq(SysUserRoleEntity::getUserId, id));
		//删除用户信息
		this.removeById(id);
		
	}
	
	@Override
	public void updateBatch(SysUserEntity user, Long[] userIds) {
		List<Long> idList = Arrays.asList(userIds);

		idList.forEach(id -> {
			SysUserEntity nowUser = new SysUserEntity();
			BeanUtil.copyProperties(user, nowUser);
			nowUser.setId(id.intValue());
			this.saveValid(nowUser);
			if (StrUtil.isNotBlank(nowUser.getPassword())) {
			
				nowUser.setPassword(Md5Util.md5(nowUser.getPassword()));

			}
			this.updateById(nowUser);

			//更新缓存
			CacheConfig.userCache.put(Long.valueOf(nowUser.getId()), nowUser);
		});
		
	}
   
	
	/**
	 * 根据用户名，查询系统用户
	 */
	@Override
	public SysUserEntity queryByUserName(String username) {
		return sysUserMapper.queryByUserName(username);
	}

    /**
     * 用户菜单列表
     */
	@Override
	public List<SysMenuEntity> queryAllMenu(int userId, String prop) {
		return sysUserMapper.queryAllMenu(userId, prop);
	}

	
	
	/**
	 * 用户保存校验
	 * @param entity 用户对象
	 */
	private void saveValid(SysUserEntity entity) {
		// 判断编码是否重复
		List<SysUserEntity> users = this.list(new QueryWrapper<SysUserEntity>().lambda()
				.eq(SysUserEntity::getUsername, entity.getUsername()));
				
		if (CollectionUtil.isNotEmpty(users)) {
			throw new RRException("编码已存在，不能保存");
		}
	}

	


	
	

}
