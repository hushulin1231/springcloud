package com.cn.provider.modules.sys.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cn.provider.common.annotation.SysLog;
import com.cn.provider.common.utils.PageUtils;
import com.cn.provider.common.utils.R;
import com.cn.provider.modules.sys.entity.SysLogEntity;
import com.cn.provider.modules.sys.form.SysDictQueryForm;
import com.cn.provider.modules.sys.service.SysLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 系统日志
 *
 * @author hsl
 */
@Api(tags={"系统日志查询"})
@RestController
@RequestMapping("/sys/log")
public class SysLogController {
	
	@Autowired
	private SysLogService sysLogService;

    /**
     * 分页查询系统日志信息
     * @param params
     * @return
     */
	@ApiOperation(value="查询系统日志信息")
	@SysLog("查询系统日志信息")
	@PostMapping("/list")
	public R<PageUtils<SysLogEntity>> list(@RequestBody SysDictQueryForm params){
		PageUtils<SysLogEntity> page = sysLogService.queryPage(params);
		return R.ok(page);
	}
	
	 /**
     * 通过id查询日志详细信息
     */
	@ApiOperation(value="通过Id查询日志详细信息")
	@SysLog("通过Id查询日志详细信息")
    @GetMapping("getById/{id}")
    public R<SysLogEntity> getById(@PathVariable("id") Long id){
    	SysLogEntity sysLogEntity = sysLogService.getById(id);
         return R.ok(sysLogEntity);
    }

}
