package com.cn.provider.modules.sys.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cn.provider.common.utils.PageInfo;
import com.cn.provider.modules.sys.entity.SysMenuEntity;
import com.cn.provider.modules.sys.entity.SysUserEntity;
import com.cn.provider.modules.sys.form.SysUserQueryForm;

public interface SysUserService extends IService<SysUserEntity>{
	
	/**
	 * 根据用户名，查询系统用户
	 */
	SysUserEntity queryByUserName(String username);
	
    /**
     * 用户菜单列表
     * @param userId
     * @param prop
     * @return
     */
	List<SysMenuEntity> queryAllMenu(int userId, String prop);

	/**
	 * 分页查询用户信息
	 * @param params
	 * @return
	 */
	PageInfo<SysUserEntity> queryPage(SysUserQueryForm params);

	/**
	 * 新增用户
	 * @param user
	 */
	void saveUser(SysUserEntity user);

	/**
	 * 修改用户信息
	 * @param user
	 */
	void update(SysUserEntity user);
	
    /**
     * 删除用户
     * @param id
     */
	void delete(int id);

	/**
	 * 重置密码
	 * @param user
	 * @param userIds
	 */
	void updateBatch(SysUserEntity user, Long[] userIds);

	

}
