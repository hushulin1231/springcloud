package com.cn.provider.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cn.provider.common.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 数据字典内容
 */
@ApiModel("数据字典内容 ")
@TableName("sys_dict_val")
@Getter
@Setter
public class SysDictValEntity  extends BaseEntity {
	
	@TableId
	private int id;

	/**
	 * 字典ID
	 */
	@ApiModelProperty(value="字典ID")
    private Long dictId;

	/**
	 * 内容编码
	 */
	@ApiModelProperty(value="内容编码")
    private String valCode;

	/**
	 * 内容名称
	 */
	@ApiModelProperty(value="内容名称")
    private String valName;

	/**
	 * 备注
	 */
	@ApiModelProperty(value="备注")
    private String memo;


	@ApiModelProperty(value="统计字段（非数据库字段）")
	@TableField(exist = false)
	private Integer count;

}
