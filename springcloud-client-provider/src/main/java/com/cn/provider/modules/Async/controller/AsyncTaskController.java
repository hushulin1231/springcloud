package com.cn.provider.modules.Async.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cn.provider.common.annotation.SysLog;
import com.cn.provider.modules.Async.task.AsyncTask;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;


@Api(tags={"多线程接口"})
@RestController
@RequestMapping(value = "/async")
@Slf4j
public class AsyncTaskController {
	
	@Autowired
	private AsyncTask asyncTask;
	
	 @ApiOperation(value="测试多线程")
	 @GetMapping(value = "/test")
	 @SysLog("测试多线程")
	 public Object test(){
	 for (int i = 0; i < 10; i++) {
	     asyncTask.register();
	 }
	 log.info("主线程结束");
	 return "OK";
	 }

}
