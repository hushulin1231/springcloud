package com.cn.provider.modules.sys.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cn.provider.modules.sys.entity.SysUserRoleEntity;

@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRoleEntity>{

	/**
	 * 获取用户所属的角色列表
	 * @param id
	 * @return
	 */
	List<Long> queryRoleIdList(int id);

    /**
     * 根据角色ID，获取用户ID列表
     * @param roleId
     * @return
     */
	List<Long> queryUserIdList(Long roleId);

    /**
     * 根据角色ID数组，批量删除
     * @param roleIds
     */
	int deleteBatch(Long[] roleIds);

}
