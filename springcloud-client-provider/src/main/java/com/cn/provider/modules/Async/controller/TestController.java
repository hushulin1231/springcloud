package com.cn.provider.modules.Async.controller;

import com.cn.provider.common.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Api(tags={"测试接口"})
@RestController
public class TestController {


    @ApiOperation(value="调用接口测试")
    @SysLog("请求接口")
    @RequestMapping(value="/hello",method = RequestMethod.GET)
    public String hello(String name){
        return name+"中国你好";
    }


}
