package com.cn.provider.modules.sys.form;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cn.provider.common.entity.PageQueryEntity;
import com.cn.provider.modules.sys.entity.SysUserEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("查询参数")
public class SysUserQueryForm extends PageQueryEntity {

    @ApiModelProperty("编码或名称关键字")
    private String key;



    /**
     * 查询条件
     */
    public LambdaQueryWrapper<SysUserEntity> queryWrapper() {
        return new LambdaQueryWrapper<SysUserEntity>()
                .and(StrUtil.isNotBlank(key), wapper -> wapper
                        .like(SysUserEntity::getUsername, this.getKey())
                        .or()
                        .like(SysUserEntity::getRealName, this.getKey())
                        .or()
                        .like(SysUserEntity::getMobile, this.getKey())
                )
                .orderByDesc(SysUserEntity::getCreateTime);
    }

}
