package com.cn.provider.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;


/**
 * 系统日志实体类

 */
@Data
@TableName("sys_log")
public class SysLogEntity  {
	
	@TableId
	private int id;

	//用户名
	@ApiModelProperty("用户名")
	private String username;
	//用户操作
	@ApiModelProperty("用户操作")
	private String operation;
	//请求方法
	@ApiModelProperty("请求方法")
	private String method;
	//请求参数
	@ApiModelProperty("请求参数")
	private String params;
	//执行时长(毫秒)
	@ApiModelProperty("执行时长(毫秒)")
	private Long time;
	//请求地址
	@ApiModelProperty("请求地址")
	private String url;
	//返回结果
	@ApiModelProperty("返回结果")
	private String resparam;
	//IP地址
	@ApiModelProperty("IP地址")
	private String ip;
	
	//接口名称
	@ApiModelProperty("接口名称")
	private String className;
	
	//创建时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@ApiModelProperty("创建时间")
	private Date createDate;

}
