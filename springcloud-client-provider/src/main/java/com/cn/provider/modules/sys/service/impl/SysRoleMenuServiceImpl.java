package com.cn.provider.modules.sys.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cn.provider.common.utils.Constant;
import com.cn.provider.modules.sys.entity.SysRoleMenuEntity;
import com.cn.provider.modules.sys.mapper.SysRoleMenuMapper;
import com.cn.provider.modules.sys.service.SysRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



/**
 * 角色与菜单对应关系
 *
 * @author 
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenuEntity> implements SysRoleMenuService {

	@Autowired
	private SysRoleMenuMapper sysRoleMenuMapper;
	
	/**
	 * 保存角色与菜单对应关系数据
	 */
	@Override
	public void saveOrUpdate(int roleId, List<Long> menuIdList) {
		//删除角色与菜单关系
        this.remove(new LambdaQueryWrapper<SysRoleMenuEntity>().in(SysRoleMenuEntity::getRoleId, roleId));
		//保存角色与菜单关系
        List<SysRoleMenuEntity> list = new ArrayList<>();
		for(Long menuId : menuIdList) {
			SysRoleMenuEntity sysRoleMenuEntity = new SysRoleMenuEntity();
			sysRoleMenuEntity.setMenuId(menuId);
			sysRoleMenuEntity.setRoleId(roleId);
			list.add(sysRoleMenuEntity);
		}
        if (CollectionUtil.isNotEmpty(list)) {
            this.saveBatch(list);
        }
	}

	@Override
	public List<Long> queryMenuIdList(Long roleId) {
		return sysRoleMenuMapper.queryMenuIdList(roleId);
	}

	@Override
	public List<Long> queryPermUserIdList(String[] perms) {
		List<Long> userIdList = baseMapper.queryPermUserIdList(new QueryWrapper<>()
				.in(perms != null && perms.length > 0, "m.perms", Arrays.asList(perms)));
		if (userIdList == null) {
			userIdList = new ArrayList<>();
		}
		userIdList.add(Constant.SUPER_ADMIN);
		return userIdList;
	}

	
   /*
    * 删除角色与菜单关联
    */
	@Override
	public void deleteBatch(Long[] roleIds) {
		this.remove(new LambdaQueryWrapper<SysRoleMenuEntity>().in(SysRoleMenuEntity::getRoleId, Arrays.asList(roleIds)));
	}

}
