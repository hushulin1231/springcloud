package com.cn.provider.modules.sys.form;

import com.cn.provider.modules.sys.entity.SysUserEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 批量编辑表单
 * @author DING
 */
@ApiModel("批量编辑表单")
@Getter
@Setter
public class SysUserUpdateBatchForm {
	
	 @ApiModelProperty("用户id数组")
	 private Long[] userIds;
	 
	 @ApiModelProperty("用户对象")
	 private SysUserEntity user;

}
