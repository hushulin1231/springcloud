package com.cn.provider.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cn.provider.common.utils.PageUtils;
import com.cn.provider.modules.sys.entity.SysDictEntity;
import com.cn.provider.modules.sys.form.SysDictQueryForm;

public interface SysDictService extends IService<SysDictEntity>{
	
    /**
     * 分页查询数据字典
     * @param params
     * @return
     */
	PageUtils<SysDictEntity> queryPage(SysDictQueryForm params);
    
	/**
	 * 删除数据字典
	 * @param ids
	 */
	void delete(Long[] ids);

}
