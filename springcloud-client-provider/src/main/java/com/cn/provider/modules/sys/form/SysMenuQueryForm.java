package com.cn.provider.modules.sys.form;


import com.cn.provider.common.utils.Constant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("查询参数")
public class SysMenuQueryForm {

    @ApiModelProperty(value = "菜单标识，默认为系统菜单", required = true, example = "sys/menu")
    private String prop = Constant.MENU_PROP_SYS;
}
