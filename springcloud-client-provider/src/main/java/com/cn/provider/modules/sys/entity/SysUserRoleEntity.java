package com.cn.provider.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;



/**
 * 用户与角色对应关系
 */
@Data
@TableName("sys_user_role")
public class SysUserRoleEntity  {
	
	@TableId
	private int id;

	/**
	 * 用户ID
	 */
	private int userId;

	/**
	 * 角色ID
	 */
	private int roleId;


}
