package com.cn.provider.modules.sys.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cn.provider.common.utils.Constant;
import com.cn.provider.common.utils.MapUtils;
import com.cn.provider.common.utils.ShiroUtils;
import com.cn.provider.config.CacheConfig;
import com.cn.provider.modules.sys.entity.SysMenuEntity;
import com.cn.provider.modules.sys.form.SysMenuQueryForm;
import com.cn.provider.modules.sys.mapper.SysMenuMapper;
import com.cn.provider.modules.sys.service.SysMenuService;
import com.cn.provider.modules.sys.service.SysRoleMenuService;
import com.cn.provider.modules.sys.service.SysUserService;




@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenuEntity> implements SysMenuService{

	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysRoleMenuService sysRoleMenuService;

	@Override
	public List<SysMenuEntity> listAll() {
		return this.list(new LambdaQueryWrapper<SysMenuEntity>());
	}
    
    /**
     * 根据父菜单，查询子菜单
     */
	@Override
	public List<SysMenuEntity> queryListParentId(Long parentId) {
		return this.list(new LambdaQueryWrapper<SysMenuEntity>()
				.eq(SysMenuEntity::getParentId, parentId)
				.orderByAsc(SysMenuEntity::getOrderNum)
		);
	}
   
	/**
	 * 获取用户菜单列表
	 */
	@Override
	public List<SysMenuEntity> getUserMenuList(SysMenuQueryForm params) {
		int userId = ShiroUtils.getUserId();
		//系统管理员，拥有最高权限
		if(userId == Constant.SUPER_ADMIN){
			return this.list(new LambdaQueryWrapper<SysMenuEntity>()
					.eq(params.getProp() != null, SysMenuEntity::getProp, params.getProp())
					.ne(SysMenuEntity::getType, Constant.MenuType.BUTTON.getValue())
					.orderByAsc(SysMenuEntity::getOrderNum)
			);
		}
		//用户菜单列表
		return sysUserService.queryAllMenu(userId, params.getProp());
	}

	@Override
	public boolean save(SysMenuEntity entity) {
		boolean ret = super.save(entity);

		//更新缓存
		CacheConfig.menuCache.put(Long.valueOf(entity.getId()), entity);

		return ret;
	}

	@Override
	public boolean updateById(SysMenuEntity entity) {
		boolean ret = super.updateById(entity);

		//更新缓存
		CacheConfig.menuCache.put(Long.valueOf(entity.getId()), entity);

		return ret;
	}
	
	/**
	 * 删除菜单
	 */
    @Transactional(rollbackFor = Exception.class)
	@Override
	public void delete(Long menuId) {
		//删除菜单
		this.removeById(menuId);
		//删除菜单与角色关联
		sysRoleMenuService.removeByMap(new MapUtils().put("menu_id", menuId));
		//更新缓存
		CacheConfig.menuCache.get(menuId);
	}

    
    /**
     * 根据父级id获取所有子集
     */
	@Override
	public List<SysMenuEntity> queryAllChildren(int parentId){
		List<SysMenuEntity> allList = new ArrayList<>();
		CacheConfig.menuCache.forEach(menu -> {
			allList.add(menu);
			
		});
		return getAllChildrenList(allList, parentId);
	}

	/**
	 * 递归查询所有子节点
	 * @param allList 菜单集合
	 * @param parentId 父级id
	 * @return 子菜单集合
	 */
	private List<SysMenuEntity> getAllChildrenList(List<SysMenuEntity> allList, int parentId) {
		List<SysMenuEntity> childrenList = new ArrayList<>();
		// 判空
		if (allList == null || allList.size() == 0) {
			return childrenList;
		}
		// 找出所有的下一级节点
		for (SysMenuEntity menu : allList) {
			if (parentId==menu.getParentId()) {
				childrenList.add(menu);
			}
		}
		// 当前所有子级集合
		List<SysMenuEntity> allChildrenList = new ArrayList<>(childrenList);
		// 遍历下一级节点
		if (childrenList.size() > 0) {
			for (SysMenuEntity menu : childrenList) {
				// 递归
				List<SysMenuEntity> children = getAllChildrenList(allList, menu.getId());
				if (children.size() > 0) {
					allChildrenList.addAll(children);
				}
			}
		}
		return allChildrenList;
	}

}
