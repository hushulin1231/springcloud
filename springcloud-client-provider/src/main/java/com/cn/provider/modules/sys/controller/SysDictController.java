package com.cn.provider.modules.sys.controller;


import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cn.provider.common.annotation.SysLog;
import com.cn.provider.common.utils.PageUtils;
import com.cn.provider.common.utils.R;
import com.cn.provider.modules.sys.entity.SysDictEntity;
import com.cn.provider.modules.sys.form.SysDictQueryForm;
import com.cn.provider.modules.sys.service.SysDictService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * 数据字典
 * @author hsl
 *
 */
@Api(tags={"数据字典"})
@RestController
@RequestMapping("sys/dict")
public class SysDictController {

    @Autowired
    private SysDictService sysDictService;

    /**
     * 分页查询数据字典
     */
    @ApiOperation(value="分页数据字典")
    @SysLog("数据字典查询")
    @GetMapping("/page")
    public R<PageUtils<SysDictEntity>> page(SysDictQueryForm params){
        PageUtils<SysDictEntity> page = sysDictService.queryPage(params);
        return R.ok(page);
    }
    
    /**
     * 新增数据字典信息
     */
    @ApiOperation(value="新增数据字典信息")
    @SysLog("新增数据字典信息")
    @PostMapping("/save")
    public R<?> save(@RequestBody SysDictEntity sysDict){
    	sysDict.setCreateTime(new Date());
        sysDictService.save(sysDict);
        return R.ok();
    }
    
    /**
     * 通过id查询详细信息
     * @param id
     * @return
     */
    @ApiOperation(value="通过id查询详细信息")
    @SysLog("通过id查询详细信息")
    @GetMapping("/info/{id}")
    public R<SysDictEntity> info(@PathVariable("id") Long id){
        SysDictEntity sysDict = sysDictService.getById(id);
        return R.ok(sysDict);
    }
    
    /**
     * 修改数据字典信息
     */
    @ApiOperation(value="修改数据字典信息")
    @SysLog("修改数据字典信息")
    @PostMapping("/update")
    public R<?> update(@RequestBody SysDictEntity sysDict){
    	sysDict.setUpdateTime(new Date());
        sysDictService.updateById(sysDict);
        return R.ok();
    }

    /**
     * 删除数据字典信息
     */
    @ApiOperation(value="删除数据字典信息")
    @SysLog("删除数据字典信息")
    @PostMapping("/delete")
    public R<?> delete(@RequestBody Long[] ids){
    	sysDictService.delete(ids);
       
        return R.ok();
    }

}
