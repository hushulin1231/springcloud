package com.cn.provider.modules.sys.controller;



import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.cn.provider.common.annotation.SysLog;
import com.cn.provider.common.utils.R;
import com.cn.provider.common.utils.ShiroUtils;
import com.cn.provider.modules.sys.entity.SysUserEntity;
import com.cn.provider.modules.sys.form.SysLoginForm;
import com.cn.provider.modules.sys.service.ShiroService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;


/**
 * 登录相关
 */
@Api(tags={"用户登录相关"})
@RestController
@RequestMapping("/login")
public class SysLoginController {
	
    @Autowired
	private  ShiroService shiroService;
  


    /**
     * 用户登录
     * @param form
     * @return
     */
    @ApiOperation(value="用户登录")
	@PostMapping("/sys")
	@SysLog("用户登录")	
	public R<?> sysLogin(@RequestBody SysLoginForm form) {
		SysUserEntity user = shiroService.login(form);
		return R.ok().put("user", user).put("token", SecurityUtils.getSubject().getSession().getId());
	}


	/**
	 * 获取登录的用户信息
	 */
    @ApiOperation(value="获取登录的用户信息")
	@SysLog("获取登录用户的信息")
	@GetMapping("/user/info")
	public R<SysUserEntity> info(){
		return R.ok(ShiroUtils.getUserEntity());
	}


	/**
	 * 后台退出登录
	 * @param request
	 * @return
	 */
    @ApiOperation(value="后台退出登录")
	@SysLog("后台退出登录")
	@PostMapping("/sys/logout")
	public R<?> sysLogout(HttpServletRequest request) {
		SecurityUtils.getSubject().logout();
		return R.ok();
	}


}
