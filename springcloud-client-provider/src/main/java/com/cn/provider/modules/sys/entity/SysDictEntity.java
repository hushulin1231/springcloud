package com.cn.provider.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cn.provider.common.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * 数据字典
 */
@ApiModel("数据字典 ")
@TableName("sys_dict")
@Getter
@Setter
public class SysDictEntity extends BaseEntity {
	
	@TableId
	private int id;

	/**
	 * 字典编码
	 */
	@ApiModelProperty(value="字典编码")
    private String dictCode;

	/**
	 * 字典名称
	 */
	@ApiModelProperty(value="字典名称")
    private String dictName;

	/**
	 * 备注
	 */
	@ApiModelProperty(value="备注")
    private String memo;	


}
