package com.cn.provider.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cn.provider.common.entity.BaseEntity;
import com.cn.provider.config.CacheConfig;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 菜单管理实体类
 */
@ApiModel("菜单")
@Getter
@Setter
@TableName("sys_menu")
public class SysMenuEntity extends BaseEntity  {

    @TableId
    private int id;
	/**
	 * 父菜单ID，一级菜单为0
	 */
	@ApiModelProperty("父菜单ID")
	private int parentId;

	/**
	 * 父菜单名称
	 */
	@ApiModelProperty("父菜单名称")
	@TableField(exist=false)
	private String parentName;

	public String getParentName() {
		return CacheConfig.getMenuCache(Long.valueOf(parentId)).getName();
	}

	/**
	 * 菜单名称
	 */
	@ApiModelProperty("菜单名称")
	private String name;

	/**
	 * 菜单URL
	 */
	@ApiModelProperty("父菜单ID")
	private String url;

	/**
	 * 组件路径
	 */
	@ApiModelProperty("父菜单ID")
	private String component;

	/**
	 * 授权(多个用逗号分隔，如：user:list,user:create)
	 */
	@ApiModelProperty("授权标识")
	private String perms;

	/**
	 * 菜单标识
	 */
	@ApiModelProperty("菜单标识")
	private String prop;

	/**
	 * 类型     0：目录   1：菜单   2：按钮
	 */
	@ApiModelProperty("菜单类型")
	private Integer type;

	/**
	 * 菜单图标
	 */
	@ApiModelProperty("菜单图标")
	private String icon;

	/**
	 * 排序
	 */
	@ApiModelProperty("排序")
	private Integer orderNum;

	/**
	 * 是否显示
	 */
	@ApiModelProperty("是否显示")
	private Integer isShow;

	/**
	 * 系统预置
	 */
	@ApiModelProperty("系统预置")
	private Integer isSys;

}
