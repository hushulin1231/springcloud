package com.cn.provider.modules.sys.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cn.provider.modules.sys.entity.SysUserRoleEntity;
import com.cn.provider.modules.sys.mapper.SysUserRoleMapper;
import com.cn.provider.modules.sys.service.SysUserRoleService;

@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRoleEntity>implements SysUserRoleService{
	
	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;

	/**
	 * 获取用户所属的角色列表
	 */
	@Override
	public List<Long> queryRoleIdList(int id) {
		return sysUserRoleMapper.queryRoleIdList(id);
	}

	
	/**
	 * 根据用户ID，新增用户角色关联表
	 */
	@Override
	public void saveRole(int userId, List<Long> roleIdList) {

		if(roleIdList == null || roleIdList.size() == 0){
			return ;
		}		
		//保存用户与角色关系
		for(Long roleId : roleIdList){
			SysUserRoleEntity sysUserRoleEntity = new SysUserRoleEntity();
			sysUserRoleEntity.setUserId(userId);
			sysUserRoleEntity.setRoleId(roleId.intValue());
			this.save(sysUserRoleEntity);

		}
	}

	/**
	 * 根据用户ID，修改用户角色关联表
	 */
	@Override
	public void updateRole(int id, List<Long> roleIdList) {
		//通过用户id删除用户角色关联表信息	
		this.remove(new LambdaQueryWrapper<SysUserRoleEntity>().eq(SysUserRoleEntity::getUserId, id));
		if(roleIdList == null || roleIdList.size() == 0){
			return ;
		}

		//保存用户与角色关系
		for(Long roleId : roleIdList){
			SysUserRoleEntity sysUserRoleEntity = new SysUserRoleEntity();
			sysUserRoleEntity.setUserId(id);
			sysUserRoleEntity.setRoleId(roleId.intValue());

			this.save(sysUserRoleEntity);

		}
	}

	/**
	 * 根据角色ID，获取用户ID列表
	 */
	@Override
	public List<Long> queryUserIdList(Long roleId) {
		return sysUserRoleMapper.queryUserIdList(roleId);
	}


	@Override
	public void saveOrUpdateRole(int roleId, List<Long> userIdList) {
		//先删除用户与角色关系
		this.remove(new LambdaQueryWrapper<SysUserRoleEntity>().eq(SysUserRoleEntity::getRoleId, roleId));

			if(userIdList == null || userIdList.size() == 0){
				return ;
			}

				//保存用户与角色关系
				for(Long userId : userIdList){
					SysUserRoleEntity sysUserRoleEntity = new SysUserRoleEntity();
					sysUserRoleEntity.setUserId(userId.intValue());
					sysUserRoleEntity.setRoleId(roleId);

					this.save(sysUserRoleEntity);
				}
		
	}

    /**
     * 删除角色与用户关联
     */
	@Override
	public int deleteBatch(Long[] roleIds) {
		return sysUserRoleMapper.deleteBatch(roleIds);
		
	}
	

}
