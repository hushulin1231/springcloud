package com.cn.provider.modules.sys.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cn.provider.modules.sys.entity.SysMenuEntity;
import com.cn.provider.modules.sys.entity.SysUserEntity;
import com.cn.provider.modules.sys.form.SysUserQueryForm;


@Mapper
public interface SysUserMapper extends BaseMapper<SysUserEntity>{
	
	/**
	 * 分页查询用户信息
	 * @param params
	 * @return
	 */
	List<SysUserEntity> page(SysUserQueryForm params);

	/**
	 *  根据用户名，查询系统用户
	 * @param username
	 * @return
	 */
	SysUserEntity queryByUserName(String username);
	
    /**
     * 查询用户的所有权限
     * @param userId
     * @param prop
     * @return
     */
	List<String> queryAllPerms(int userId, String prop);

    /**
     * 用户菜单列表
     * @param userId
     * @param prop
     * @return
     */
	List<SysMenuEntity> queryAllMenu(int userId, String prop);

	

}
