package com.cn.provider.modules.sys.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cn.provider.modules.sys.entity.SysUserRoleEntity;

public interface SysUserRoleService extends IService<SysUserRoleEntity>{
	
	/**
	 * 根据用户ID，获取角色ID列表
	 */
	List<Long> queryRoleIdList(int id);

	/**
	 * 根据用户ID，新增用户角色关联表
	 * @param id
	 * @param roleIdList
	 */
	void saveRole(int id, List<Long> roleIdList);

	/**
	 * 根据用户ID，修改用户角色关联表
	 * @param id
	 * @param roleIdList
	 */
	void updateRole(int id, List<Long> roleIdList);
	
	/**
	 * 根据角色ID，获取用户ID列表
	 */
	List<Long> queryUserIdList(Long roleId);

	/**
	 * 根据角色ID，修改用户角色关联表
	 */
	void saveOrUpdateRole(int roleId, List<Long> userIdList);

	/**
	 * 删除角色与用户关联
	 * @param roleIds
	 */
	int deleteBatch(Long[] roleIds);

}
