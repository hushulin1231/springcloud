package com.cn.provider.modules.sys.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cn.provider.common.utils.PageUtils;
import com.cn.provider.common.utils.Query;
import com.cn.provider.modules.sys.entity.SysLogEntity;
import com.cn.provider.modules.sys.form.SysDictQueryForm;
import com.cn.provider.modules.sys.mapper.SysLogMapper;
import com.cn.provider.modules.sys.service.SysLogService;
import cn.hutool.core.util.StrUtil;

@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLogEntity>implements SysLogService{
    
	/**
	 * 分页查询日志信息
	 */
	@Override
	public PageUtils<SysLogEntity> queryPage(SysDictQueryForm params) {
		 String key = params.getKey();

	        IPage<SysLogEntity> page = this.page(
	            new Query<SysLogEntity>().getPage(params),
	            new QueryWrapper<SysLogEntity>().lambda()
	                    .and(StrUtil.isNotBlank(key), wrapper -> wrapper
	                            .like(SysLogEntity::getUsername, key)
	                            .or()
	                            .like(SysLogEntity::getOperation, key)
	                    )
	                    .orderByDesc(SysLogEntity::getCreateDate)
	        );

	        return new PageUtils<>(page);
	}

	

}
