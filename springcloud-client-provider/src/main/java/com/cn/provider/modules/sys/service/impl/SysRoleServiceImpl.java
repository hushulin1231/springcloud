package com.cn.provider.modules.sys.service.impl;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cn.provider.common.utils.PageUtils;
import com.cn.provider.common.utils.Query;
import com.cn.provider.modules.sys.entity.SysRoleEntity;
import com.cn.provider.modules.sys.form.SysRoleQueryForm;
import com.cn.provider.modules.sys.mapper.SysRoleMapper;
import com.cn.provider.modules.sys.service.SysRoleMenuService;
import com.cn.provider.modules.sys.service.SysRoleService;
import com.cn.provider.modules.sys.service.SysUserRoleService;


@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRoleEntity> implements SysRoleService{

	
	@Autowired
    private  SysRoleMenuService sysRoleMenuService;
	
	@Autowired
    private  SysUserRoleService sysUserRoleService;
	
	/**
	 * 分页查询角色列表
	 */
	@Override
	public PageUtils<SysRoleEntity> queryPage(SysRoleQueryForm params) {
		IPage<SysRoleEntity> page = this.page(new Query<SysRoleEntity>().getPage(params), params.queryWrapper());

		return new PageUtils<>(page);
	}
	
	
    /**
     * 修改角色
     */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(SysRoleEntity role) {
	    this.updateById(role);
	    //保存角色与菜单关系
        if (role.getMenuIdList() != null) {
        sysRoleMenuService.saveOrUpdate(role.getRoleId(), role.getMenuIdList());
        }
		
	}
	
    /**
     * 删除角色
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteBatch(Long[] roleIds) {
        //删除角色
        this.removeByIds(Arrays.asList(roleIds));

        //删除角色与菜单关联
        sysRoleMenuService.deleteBatch(roleIds);

        //删除角色与用户关联
        sysUserRoleService.deleteBatch(roleIds);
    }
    



	

}
