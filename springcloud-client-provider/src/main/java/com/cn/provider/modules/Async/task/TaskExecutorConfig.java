package com.cn.provider.modules.Async.task;

import java.util.concurrent.Executor;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class TaskExecutorConfig implements AsyncConfigurer{

	 private static final int CORE_POOL_SIZE = 2;

	 private static final int MAX_POOL_SIZE = 5;

	 private static final int QUEUE_CAPACITY = 100;
	 /**
	 * 通过重写getAsyncExecutor方法，制定默认的任务执行由该方法产生
	 *
	 * 配置类实现AsyncConfigurer接口并重写getAsyncExcutor方法，并返回一个ThreadPoolTaskExevutor
	 * 这样我们就获得了一个基于线程池的TaskExecutor
	 */
	 @Override
	 public Executor getAsyncExecutor() {
	 ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
	 taskExecutor.setCorePoolSize(CORE_POOL_SIZE);
	 taskExecutor.setMaxPoolSize(MAX_POOL_SIZE);
	 taskExecutor.setQueueCapacity(QUEUE_CAPACITY);
	 taskExecutor.initialize();
	 return taskExecutor;
	 }
}
