package com.cn.provider.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cn.provider.common.entity.BaseEntity;
import com.cn.provider.common.utils.ShiroUtils;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

/**
 * 系统用户实体类
 */
@TableName("sys_user")
@Getter
@Setter
public class SysUserEntity extends BaseEntity {

    /**
     * 主剪id
     */
	@TableId(value = "id", type = IdType.AUTO)
    private int id;

	//姓名
	private String realName;

	//手机号
	private String mobile;

    //用户名
	private String username;

	//密码
	private String password;

    //状态  0：禁用   1：正常
	private Integer status;

    //创建人id
    private int creator;
    
	@TableField(exist=false)
	private String creatorName;
	
	//获取创建人姓名
	public String getCreatorName() {
		return ShiroUtils.getUserEntity().getRealName();
	}
     

    //角色ID列表
	@TableField(exist=false)
	private List<Long> roleIdList;

}
