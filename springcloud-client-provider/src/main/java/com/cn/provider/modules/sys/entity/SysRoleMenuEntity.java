package com.cn.provider.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

/**
 * 角色与菜单对应关系实体类
 */
@Getter
@Setter
@TableName("sys_role_menu")
public class SysRoleMenuEntity {

	@TableId
	private int id;

	/**
	 * 角色ID
	 */
	private int roleId;

	/**
	 * 菜单ID
	 */
	private Long menuId;

}
