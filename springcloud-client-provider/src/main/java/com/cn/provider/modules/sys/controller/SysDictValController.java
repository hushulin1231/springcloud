package com.cn.provider.modules.sys.controller;

import java.util.Arrays;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cn.provider.common.annotation.SysLog;
import com.cn.provider.common.utils.PageUtils;
import com.cn.provider.common.utils.R;
import com.cn.provider.modules.sys.entity.SysDictValEntity;
import com.cn.provider.modules.sys.form.SysDictValQueryForm;
import com.cn.provider.modules.sys.service.SysDictValService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 数据字典内容
 * @author hsl
 *
 */
@Api(tags={"数据字典内容"})
@RestController
@RequestMapping("sys/dictval")
public class SysDictValController {

	 @Autowired
	 private SysDictValService sysDictValService;

	    /**
	     * 分页查询数据字典内容
	     */
	    @ApiOperation(value="分页查询数据字典内容")
	    @SysLog("查询数据字典内容")
	    @GetMapping("/page")
	    public R<PageUtils<SysDictValEntity>> page(SysDictValQueryForm params){
	        PageUtils<SysDictValEntity> page = sysDictValService.queryPage(params);
	        return R.ok(page);
	    }
	    

	    /**
	     * 新增数据字典信息
	     */
	    @ApiOperation(value="新增数据字典信息")
	    @SysLog("新增数据字典信息")
	    @PostMapping("/save")
	    public R<?> save(@RequestBody SysDictValEntity sysDictVal){
	    	sysDictVal.setCreateTime(new Date());
	        sysDictValService.save(sysDictVal);
	        return R.ok();
	    }
	    
	    /**
	     * 通过Id查询详细数据
	     */
	    @ApiOperation(value="通过Id查询详细数据")
	    @SysLog("通过Id查询详细数据")
	    @GetMapping("/info/{id}")
	    public R<SysDictValEntity> info(@PathVariable("id") Long id){
	        SysDictValEntity sysDictVal = sysDictValService.getById(id);
	        return R.ok(sysDictVal);
	    }
	    
	    
	    /**
	     * 修改数据字典信息
	     */
	    @ApiOperation(value="修改数据字典信息")
	    @SysLog("修改数据字典信息")
	    @PostMapping("/update")
	    public R<?> update(@RequestBody SysDictValEntity sysDictVal){
	    	sysDictVal.setUpdateTime(new Date());
	        sysDictValService.updateById(sysDictVal);

	        return R.ok();
	    }

	    /**
	     * 删除数据字典信息
	     */
	    @ApiOperation(value="删除数据字典信息")
	    @SysLog("删除数据字典信息")
	    @PostMapping("/delete")
	    public R<?> delete(@RequestBody Long[] ids){
	        sysDictValService.removeByIds(Arrays.asList(ids));

	        return R.ok();
	    }


}
