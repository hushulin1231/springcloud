package com.cn.provider.modules.sys.form;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cn.provider.common.entity.PageQueryEntity;
import com.cn.provider.modules.sys.entity.SysRoleEntity;
import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("查询参数")
public class SysRoleQueryForm extends PageQueryEntity{

    @ApiModelProperty("编码或名称关键字")
    private String key;



    /**
     * 查询条件
     */
    public LambdaQueryWrapper<SysRoleEntity> queryWrapper() {
        return new LambdaQueryWrapper<SysRoleEntity>()
                .and(StrUtil.isNotBlank(key), wapper -> wapper
                        .like(SysRoleEntity::getRoleName, this.getKey())
                        )
                .orderByDesc(SysRoleEntity::getCreateTime);
    }

}
