package com.cn.provider.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cn.provider.common.utils.ShiroUtils;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;


/**
 * 角色实体类
 */
@ApiModel("角色")
@Data
@TableName("sys_role")
public class SysRoleEntity  {


	@ApiModelProperty("角色ID")
	@TableId
	private int roleId;

	@ApiModelProperty("角色名称")
	@NotBlank(message="角色名称不能为空")
	private String roleName;

	@ApiModelProperty("备注")
	private String remark;


	@ApiModelProperty("创建人ID")
	private int createUserId;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@ApiModelProperty("创建时间")
	private Date createTime;
	
	@ApiModelProperty("创建人姓名")
	@TableField(exist=false)
	private String creatorName;
	
	
	//获取创建人姓名
	public String getCreatorName() {
		return ShiroUtils.getUserEntity().getRealName();
	}

	@ApiModelProperty("菜单权限集合")
	@TableField(exist=false)
	private List<Long> menuIdList;

	@ApiModelProperty("关联用户集合")
	@TableField(exist=false)
	private List<Long> userIdList;





}
