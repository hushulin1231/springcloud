package com.cn.provider.modules.sys.form;


import com.cn.provider.common.entity.PageQueryEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("查询参数")
public class SysDictValQueryForm extends PageQueryEntity {

    @ApiModelProperty(value = "字典类型ID", required = false, example = "1")
    private Long dictId;

    @ApiModelProperty(value = "编码或名称关键字", required = false, example = "abc")
    private String key;
}
