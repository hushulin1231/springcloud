package com.cn.provider.modules.sys.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 登录请求实体类
 * @author 联想
 *
 */
@ApiModel("登录表单")
@Data
public class SysLoginForm {
	
	@ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("密码")
    private String password;

}
