package com.cn.provider.modules.sys.mapper;

import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cn.provider.modules.sys.entity.SysRoleEntity;

@Mapper
public interface SysRoleMapper extends BaseMapper<SysRoleEntity>{

}
