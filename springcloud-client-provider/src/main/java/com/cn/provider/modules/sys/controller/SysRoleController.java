package com.cn.provider.modules.sys.controller;

import java.util.Date;
import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cn.provider.common.annotation.SysLog;
import com.cn.provider.common.utils.PageUtils;
import com.cn.provider.common.utils.R;
import com.cn.provider.common.utils.ShiroUtils;
import com.cn.provider.modules.sys.entity.SysRoleEntity;
import com.cn.provider.modules.sys.form.SysRoleQueryForm;
import com.cn.provider.modules.sys.service.SysRoleMenuService;
import com.cn.provider.modules.sys.service.SysRoleService;
import com.cn.provider.modules.sys.service.SysUserRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@Api(tags={"角色配置"})
@RestController
@RequestMapping("/sys/role")
public class SysRoleController {
	
	@Autowired
	private SysRoleService sysRoleService;
	
	@Autowired
	private SysUserRoleService sysUserRoleService;
	
	@Autowired
	private SysRoleMenuService sysRoleMenuService;

	/**
	 * 分页查询角色列表
	 * @param params
	 * @return
	 */
	@ApiOperation(value = "分页查询角色列表")
	@GetMapping("/list")
	@SysLog("查询角色列表")
	public R<?> list(SysRoleQueryForm params){
		PageUtils<SysRoleEntity> page = sysRoleService.queryPage(params);
		return R.ok(page);
	}
	/**
	 * 角色列表
	 * @return
	 */
	@ApiOperation(value = "角色列表")
	@GetMapping("/select")
	@SysLog("获取角色列表")
	public R<List<SysRoleEntity>> select(){
		List<SysRoleEntity> list = sysRoleService.list();

		return R.ok(list);
	}
	
	/**
	 * 通过Id获取角色信息
	 * @param roleId
	 * @return
	 */
	@GetMapping("/info/{roleId}")
	@SysLog("通过Id获取角色信息")
	public R<SysRoleEntity> info(@PathVariable("roleId") Long roleId){
		SysRoleEntity role = sysRoleService.getById(roleId);

		//查询角色对应的菜单
		List<Long> menuIdList = sysRoleMenuService.queryMenuIdList(roleId);
		role.setMenuIdList(menuIdList);
		//查询角色对应的用户
		List<Long> userIdList = sysUserRoleService.queryUserIdList(roleId);
		role.setUserIdList(userIdList);

		return R.ok(role);
	}


	/**
	 * 新增角色
	 * @param role
	 * @return
	 */
	@SysLog("新增角色")
	@PostMapping("/save")
	@RequiresPermissions("sys:role:save")
	public R<?> save(@RequestBody SysRoleEntity role){
		role.setCreateUserId(ShiroUtils.getUserId());
	    role.setCreateTime(new Date());
	    sysRoleService.save(role);
		return R.ok();
	}


	/**
	 * 修改角色
	 * @param role
	 * @return
	 */
	@SysLog("修改角色")
	@PostMapping("/update")
	@RequiresPermissions("sys:role:update")
	public R<?> update(@RequestBody SysRoleEntity role){ 
		sysRoleService.update(role);
		return R.ok();
	}

	/**
	 *  删除角色
	 * @param roleIds
	 * @return
	 */
	@SysLog("删除角色")
	@PostMapping("/delete")
	@RequiresPermissions("sys:role:delete")
	public R<?> delete(@RequestBody Long[] roleIds){
		sysRoleService.deleteBatch(roleIds);
		return R.ok();
	}

}
