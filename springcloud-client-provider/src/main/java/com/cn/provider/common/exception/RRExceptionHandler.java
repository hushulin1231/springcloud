package com.cn.provider.common.exception;


import com.cn.provider.common.enums.REnum;
import com.cn.provider.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 异常处理器
 */
@RestControllerAdvice
@Slf4j
public class RRExceptionHandler {
    /**
     * 处理自定义异常
     */
    @ExceptionHandler(RRException.class)
    public R<?> handleRRException(RRException e)  {
        log.error(e.getMessage());
        return R.error(e.getCode(), e.getMessage()).desc(e.getDesc());
    }


    @ExceptionHandler(Exception.class)
    public R<?> handleException(Exception e) {
        log.error(e.getMessage(), e);
        return R.error().desc(e.getMessage());
    }

    @ExceptionHandler(DuplicateKeyException.class)
    public R<?> handleDuplicateKeyException (DuplicateKeyException e) {
        if(e.getMessage().contains("username")){
            // 打印出参
            log.info("【返回 参数】 : {}", R.error("该账户已经注册过了").desc(e.getMessage()));
            return R.error("该账户已经注册过了").desc(e.getMessage());
        }
        return R.error("不能添加重复数据").desc(e.getMessage());
    }
    @ExceptionHandler(UnknownAccountException.class)
    public R<?> handleUnknownAccountException(UnknownAccountException e) {
        log.error(e.getMessage());
        return R.error(REnum.UN_AUTH.getCode(), "用户不存在").desc(e.getMessage());
    }

    @ExceptionHandler(IncorrectCredentialsException.class)
    public R<?> handleIncorrectCredentialsException(IncorrectCredentialsException e) {
        log.error(e.getMessage());
        return R.error(REnum.UN_AUTH.getCode(), "账号或密码错误").desc(e.getMessage());
    }

    @ExceptionHandler(LockedAccountException.class)
    public R<?> handleLockedAccountException(LockedAccountException e) {
        log.error(e.getMessage());
        return R.error(REnum.UN_AUTH.getCode(), "账号已被锁定,请联系管理员").desc(e.getMessage());
    }

    @ExceptionHandler(UnauthenticatedException.class)
    public R<?> handleUnauthenticatedException(UnauthenticatedException e) {
        log.error(e.getMessage());
        return R.error(REnum.UN_LOGIN.getCode(), "未获取到用户认证信息").desc(e.getMessage());
    }

    @ExceptionHandler(AuthorizationException.class)
    public R<?> handleAuthorizationException(AuthorizationException e) {
        log.error(e.getMessage());
        return R.error(REnum.UN_AUTH.getCode(), "没有权限，请联系管理员授权").desc(e.getMessage());
    }


}
