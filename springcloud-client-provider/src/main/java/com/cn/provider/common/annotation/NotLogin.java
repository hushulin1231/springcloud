package com.cn.provider.common.annotation;

import java.lang.annotation.*;

/**
 * 无需登录注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NotLogin {

	boolean value() default true;
}
