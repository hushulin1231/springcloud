package com.cn.provider.common.utils;


import org.apache.shiro.SecurityUtils;
import com.cn.provider.common.enums.REnum;
import com.cn.provider.common.exception.RRException;
import com.cn.provider.modules.sys.entity.SysUserEntity;

/**
 * Shiro工具类

 */
public class ShiroUtils {


	public static SysUserEntity getUserEntity() {
		try {
			return (SysUserEntity)SecurityUtils.getSubject().getPrincipal();
		} catch (Exception e) {
			throw new RRException("获取认证信息失败", REnum.UN_LOGIN.getCode());
		}
	}

	public static int getUserId() {
		try {
			return getUserEntity().getId();
		} catch (Exception e) {
			throw new RRException("获取认证信息失败", REnum.UN_LOGIN.getCode());
		}
	}

	


}
