package com.cn.provider.common.shiro;

import com.cn.provider.common.shiro.filter.LoginFilter;
import com.cn.provider.common.shiro.oauth2.OAuth2Realm;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.mgt.DefaultSessionManager;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.servlet.Filter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Shiro配置
 *
 */
@Configuration
public class ShiroConfig {

	@Bean
	public SecurityManager securityManager(OAuth2Realm oAuth2Realm) {
		DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
		securityManager.setRealm(oAuth2Realm);
		securityManager.setRememberMeManager(null);
		securityManager.setSessionManager(sessionManager());
		return securityManager;
	}

	/**
	 * 注入session管理器
	 */
	@Bean
	public SessionManager sessionManager() {
		DefaultSessionManager sessionManager = new ShiroSessionManager();
		// 定时清除无效的session
		sessionManager.setSessionValidationSchedulerEnabled(true);
		// 删除无效的session
		sessionManager.setDeleteInvalidSessions(true);
		// 全局会话超时时间（单位毫秒），默认30分钟
		sessionManager.setGlobalSessionTimeout(1000 * 60 * 30);
		return sessionManager;
	}


	/**
	 * 注入自定义的Realm类
	 **/
	@Bean
	public OAuth2Realm oAuth2Realm() {
		return new OAuth2Realm();
	}

	@Bean("shiroFilter")
	public ShiroFilterFactoryBean shirFilter(SecurityManager securityManager) {
		ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
		shiroFilter.setSecurityManager(securityManager);
		// oauth过滤
		Map<String, Filter> filters = new HashMap<>();
		filters.put("authc", new LoginFilter());
		shiroFilter.setFilters(filters);

		Map<String, String> filterMap = new LinkedHashMap<>();
		filterMap.put("/webjars/**", "anon");

		filterMap.put("/druid/**", "anon");
		filterMap.put("/login/**", "anon");
		filterMap.put("/swagger/**", "anon");
		filterMap.put("/v2/api-docs", "anon");
		filterMap.put("/swagger-ui.html", "anon");
		filterMap.put("/swagger-resources/**", "anon");
		filterMap.put("/user/**", "anon");
		filterMap.put("/sys/**", "anon");
		filterMap.put("/**", "authc");
		shiroFilter.setFilterChainDefinitionMap(filterMap);

		return shiroFilter;
	}

	@Bean("lifecycleBeanPostProcessor")
	public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
		return new LifecycleBeanPostProcessor();
	}

	@Bean
	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
		AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
		advisor.setSecurityManager(securityManager);
		return advisor;
	}

}
