package com.cn.provider.common.shiro.filter;

import com.alibaba.fastjson.JSONObject;
import com.cn.provider.common.utils.R;
import com.cn.provider.modules.sys.entity.SysUserEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;


/**
 * 登录过滤处理
 **/
@Slf4j
public class LoginFilter extends FormAuthenticationFilter {

    /**
     * 父级处理未明确区分是未登录还是权限验证失败
     * 重写该方法，加入相应错误标识
     * AuthenticationFilter 中 只判断了 subject.isAuthenticated();
     * AuthenticatingFilter 中 追加判断了 (!isLoginRequest(request, response) && isPermissive(mappedValue))
     * 这里通过处理将两种情况区分开来，以便后续处理返回对应的提示
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        Subject subject = getSubject(request, response);
        // 未登录
        if (!subject.isAuthenticated()) {
        	  // 有错误信息，直接处理返回
              response.setCharacterEncoding("UTF-8");
			  request.setAttribute("R", R.needLogin()); 
			  return false;
			 
        }
        Session session = subject.getSession();
        log.debug("Filter session id:" + session.getId());
        log.debug("Filter session attrs:" + session.getAttributeKeys().toString());
        SysUserEntity user = (SysUserEntity) subject.getPrincipal();
        log.debug("Filter user:" + user.getId());
        session.setAttribute("uid", user.getId());
        if(mappedValue != null) {
            // 权限验证
            if (!(!isLoginRequest(request, response) && isPermissive(mappedValue))) {
                request.setAttribute("R", R.needPermit());
                return false;
            }
        }
        return true;
    }

    /**
     * 所有请求都会经过的方法。
     */
    @Override
    protected boolean onAccessDenied(ServletRequest request,
                                     ServletResponse response) throws Exception {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        if (isLoginRequest(request, response)) {
            // 登录请求
            if (isLoginSubmission(request, response)) {
                // POST 登录认证请求
                return executeLogin(request, response);
            } else {
                // 多页面web应用，放行后跳转login页面
                if (log.isTraceEnabled()) {
                    log.trace("Login page view.");
                }
                // allow them to see the login page ;)
                return true;
            }
        } else {
            // 其它请求
            // 检查attr是否有错误信息数据
            Object d = req.getAttribute("R");
            log.info(d.toString());
            if (d instanceof R) {
                log.debug("OnAccessDenied: " + ((R<?>) d).getCode() + " " + ((R<?>) d).getMsg());
                // 有错误信息，直接处理返回
				response.setCharacterEncoding("UTF-8");
                String json = JSONObject.toJSONString(d);
                PrintWriter out = res.getWriter();
                out.write(json);
                out.flush();
                out.close();
            }
            // 需要兼容传统多页面web应用，这里要经过判断跳转登录页面
            // saveRequestAndRedirectToLogin(request, response);
            return false;
        }
    }

    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        httpResponse.setHeader("Access-control-Allow-Origin", "*");
        httpResponse.setHeader("Access-Control-Allow-Methods", "*");
        httpResponse.setHeader("Access-Control-Allow-Headers", "*");

        // 放行所有OPTIONS请求
        if (httpRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
            httpResponse.setStatus(HttpStatus.OK.value());
        }
        return super.preHandle(request, response);
    }

}
