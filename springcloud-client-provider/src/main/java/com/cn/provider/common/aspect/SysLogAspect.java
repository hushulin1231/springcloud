package com.cn.provider.common.aspect;


import com.cn.provider.common.annotation.SysLog;
import com.cn.provider.common.utils.ShiroUtils;
import com.cn.provider.modules.sys.entity.SysLogEntity;
import com.cn.provider.modules.sys.entity.SysUserEntity;
import com.cn.provider.modules.sys.service.SysLogService;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Objects;


/**
 * 系统日志，切面处理类
 *
 * @author hsl
 */
@Slf4j
@Aspect
@Component
public class SysLogAspect {
	
	    @Autowired
	    private SysLogService sysLogService;

	    @Pointcut("execution(* com.cn.provider.modules.*.controller.*.*(..))")
	    private void logPointCut(){};


		@Around("logPointCut()")
	    public Object around(ProceedingJoinPoint point) throws Throwable {
			// 开始打印请求日志
			ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();

			HttpServletRequest request = Objects.requireNonNull(attributes).getRequest();
			 Object[] arguments = point.getArgs();
            // 打印请求相关参数
			log.info("========================================== Start ==========================================");
			long startTime = System.currentTimeMillis();
			log.info("【请求 URL】：{}", request.getRequestURL());
			log.info("【请求 方式】   : {}", request.getMethod());
			log.info("【请求类名】：{}，【请求方法名】：{}", point.getSignature().getDeclaringTypeName(), point.getSignature().getName());
			// 打印请求入参
			try {
				log.info("【请求 参数】   : {}", new Gson().toJson(point.getArgs()));
			} catch (Exception e) {
				// TODO: handle exception
			}			
			String targetName = point.getTarget().getClass().getName();
			Class<?> targetClass = Class.forName(targetName);
			Method[] methods = targetClass.getMethods();
		
			Object result = point.proceed();
			// 打印出参
			log.info("【返回 参数】 : {}", new Gson().toJson(result));
			// 执行耗时
			log.info("接口耗时 : {} ms", System.currentTimeMillis() - startTime);
			SysLogEntity logEntity=new SysLogEntity();
			for (Method method : methods) {
				if (method.getName().equals(point.getSignature().getName())) {
					@SuppressWarnings("rawtypes")
					Class[] classes = method.getParameterTypes();
	                if (classes.length == arguments.length) {
	                	try {
	                		//用户操作
	    					String operation = method.getAnnotation(SysLog.class).value();
	    					log.info("【操作名称】   : {}", operation);
		                	SysUserEntity user = ShiroUtils.getUserEntity();
		                	if(user!=null) {
		                		logEntity.setUsername(user.getUsername());
		                	}	      				
		      				logEntity.setMethod(request.getMethod());		      			
		      				logEntity.setParams(new Gson().toJson(point.getArgs())); 
		      				logEntity.setResparam( new Gson().toJson(result));
		      				logEntity.setUrl(request.getRequestURL().toString());
		      				logEntity.setIp(request.getRemoteAddr()); 
		      				logEntity.setCreateDate(new Date()); 
		      				logEntity.setTime(System.currentTimeMillis() - startTime); 
		      				logEntity.setClassName(point.getSignature().getDeclaringTypeName()+"."+ point.getSignature().getName());
		      			    logEntity.setOperation(operation);
		      				 //记录日志 
		      				sysLogService.save(logEntity); 
		                     break;
						} catch (Exception e) {
							// TODO: handle exception
						}
	                }
				
				}
			}
			log.info("========================================== End ==========================================");
			return result;

	    }

}
