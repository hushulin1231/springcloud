package com.cn.provider.common.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cn.provider.common.entity.PageQueryEntity;

import java.util.Map;

/**
 * 分页查询参数
 *
 * @author Mark sunlightcs@gmail.com
 */
public class Query<T> {

    /**
     * 从map中获取分页信息，组装为分页对象
     * @param params map条件参数
     * @return 分页对象
     */
    public IPage<T> getPage(Map<String, Object> params) {

        //分页对象
        Page<T> page = new Page<>(Constant.INIT_PAGE, Constant.INIT_LIMIT);

        if(params.get(Constant.PAGE) != null){
            page.setCurrent(Long.parseLong((String)params.get(Constant.PAGE)));
        }
        if(params.get(Constant.LIMIT) != null){
            page.setSize(Long.parseLong((String)params.get(Constant.LIMIT)));
        }

        //分页参数
        params.put(Constant.PAGE, page);

        return page;
    }

    /**
     * 从查询表单对象中获取分页信息，组装为分页对象
     * @param queryForm 查询表单对象
     * @return 分页对象
     */
    public IPage<T> getPage(PageQueryEntity queryForm) {
        if (queryForm.getPage() == null) {
            queryForm.setPage(Constant.INIT_PAGE);
        }
        if (queryForm.getLimit() == null) {
            queryForm.setLimit(Constant.INIT_LIMIT);
        }

        //分页对象
        return new Page<>(queryForm.getPage(), queryForm.getLimit());
    }

    /**
     * 自定义是否查询count
     */
    public IPage<T> getPage(PageQueryEntity queryForm, boolean searchCount) {
        if (queryForm.getPage() == null) {
            queryForm.setPage(Constant.INIT_PAGE);
        }
        if (queryForm.getLimit() == null) {
            queryForm.setLimit(Constant.INIT_LIMIT);
        }
        Page<T> page = new Page<>(queryForm.getPage(), queryForm.getLimit());
        page.setSearchCount(searchCount);
        page.setOptimizeCountSql(searchCount);
        return page;
    }
}
