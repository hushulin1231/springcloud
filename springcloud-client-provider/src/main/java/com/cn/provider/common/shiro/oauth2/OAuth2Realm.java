package com.cn.provider.common.shiro.oauth2;

import cn.hutool.core.util.ObjectUtil;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cn.provider.modules.sys.entity.SysUserEntity;
import com.cn.provider.modules.sys.form.SysMenuQueryForm;
import com.cn.provider.modules.sys.service.ShiroService;
import com.cn.provider.modules.sys.service.SysUserService;
import java.util.Set;

/**
 * 认证

 */
@Component
public class OAuth2Realm extends AuthorizingRealm {

    @Autowired
    private ShiroService shiroService;
    @Autowired
    private SysUserService sysUserService;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof UsernamePasswordToken;
    }

    /**
     * 授权(验证权限时调用)
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        //用户权限列表
        SysMenuQueryForm queryForm = new SysMenuQueryForm();
        queryForm.setProp(null);
        Set<String> permsSet = shiroService.getUserPermissions(queryForm);

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setStringPermissions(permsSet);
        return info;
    }

    /**
     * 认证(登录时调用)
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken paramAuthenticationToken) throws AuthenticationException {
        // 获取认证token对象
        UsernamePasswordToken token = (UsernamePasswordToken) paramAuthenticationToken;

        // 获取用户对象
        SysUserEntity user = sysUserService.queryByUserName(token.getUsername());

        // 用户不存在
        if (ObjectUtil.isEmpty(user)) {
            return null;
        }


        // 认证
        return new SimpleAuthenticationInfo(user, user.getPassword(), getName());
    }
}
