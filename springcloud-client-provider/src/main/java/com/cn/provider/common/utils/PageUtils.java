package com.cn.provider.common.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

/**
 * 分页工具类

 */

@ApiModel("分页数据")
@Data
public class PageUtils<T>  {

	@ApiModelProperty("总记录数")
	private int totalCount;

	@ApiModelProperty("每页记录数")
	private int pageSize;

	@ApiModelProperty("总页数")
	private int totalPage;

	@ApiModelProperty("当前页数")
	private int currPage;

	@ApiModelProperty("列表数据")
	private List<T> list;


	/**
	 * 分页
	 */
	public PageUtils(IPage<T> page) {
		this.list = page.getRecords();
		this.totalCount = (int)page.getTotal();
		this.pageSize = (int)page.getSize();
		this.currPage = (int)page.getCurrent();
		this.totalPage = (int)page.getPages();
	}


}
