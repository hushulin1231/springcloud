package com.cn.provider.common.utils;

public class Constant {
    /**
     * 默认页码
     */
    public static final long INIT_PAGE = 1;
    /**
     * 默认每页显示记录数（最大500）
     */
    public static final long INIT_LIMIT = 10;

    /** 超级管理员ID */
    public static final long SUPER_ADMIN = 1;
    
    /**
     * 当前页码字段
     */
    public static final String PAGE = "page";
    /**
     * 每页显示记录数字段
     */
    public static final String LIMIT = "limit";


    /**
     * 系统菜单标识
     */
    public static final String MENU_PROP_SYS = "sys/menu";
    
    //保存用户信息的session
    public static final String CURRENT_USER = "CURRENT_USER";
    
    /**
	 * 菜单类型
	 */
    public enum MenuType {
        /**
         * 目录
         */
    	CATALOG(0),
        /**
         * 菜单
         */
        MENU(1),
        /**
         * 按钮
         */
        BUTTON(2);

        private int value;

        MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
